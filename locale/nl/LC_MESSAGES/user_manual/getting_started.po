# Dutch translations for Krita Manual package
# Nederlandse vertalingen voor het pakket Krita Manual.
# Copyright (C) licensed under the GNU Free Documentation License 1.3+ unless stated otherwise
# This file is distributed under the same license as the Krita Manual package.
#
# Automatically generated, 2019.
# Freek de Kruijf <freekdekruijf@kde.nl>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: Krita Manual 4.1\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-05-04 03:33+0200\n"
"PO-Revision-Date: 2019-05-07 10:52+0200\n"
"Last-Translator: Freek de Kruijf <freekdekruijf@kde.nl>\n"
"Language-Team: Dutch <kde-i18n-nl@kde.org>\n"
"Language: nl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 19.04.0\n"

#: ../../user_manual/getting_started.rst:5
msgid "Getting Started"
msgstr "Hoe te beginnen"

#: ../../user_manual/getting_started.rst:7
msgid ""
"Welcome to the Krita Manual! In this section, we'll try to get you up to "
"speed."
msgstr ""
"Welkom in de handleiding van Krita! In deze sectie proberen we u op snelheid "
"te brengen."

#: ../../user_manual/getting_started.rst:9
msgid ""
"If you are familiar with digital painting, we recommend checking out the :"
"ref:`introduction_from_other_software` category, which contains guides that "
"will help you get familiar with Krita by comparing its functions to other "
"software."
msgstr ""
"Als u bekend bent met digitaal schilderen, dan bevelen we aan de categorie :"
"ref:`introduction_from_other_software` te lezen, die u helpt om op bekend te "
"worden met Krita door zijn functies met andere software te vergelijken."

#: ../../user_manual/getting_started.rst:11
msgid ""
"If you are new to digital art, just start with :ref:`installation`, which "
"deals with installing Krita, and continue on to :ref:`starting_with_krita`, "
"which helps with making a new document and saving it, :ref:`basic_concepts`, "
"in which we'll try to quickly cover the big categories of Krita's "
"functionality, and finally, :ref:`navigation`, which helps you find basic "
"usage help, such as panning, zooming and rotating."
msgstr ""
"Als u nieuw bent in digitale kunst, begin dan met :ref:`installation`, die "
"zich bezig houdt met Krita installeren en doorgaat tot :ref:"
"`starting_with_krita`, wat helpt met het maken van een nieuw document en het "
"op te slaan, :ref:`basic_concepts`, waarin we zullen proberen snel de de "
"belangrijkste categorieën van de functionaliteit van Krita te dekken en "
"tenslotte, :ref:`navigation`, wat u helpt om hulp te zoeken bij basis "
"gebruik, zoals rondkijken, zoomen en draaien."

#: ../../user_manual/getting_started.rst:13
msgid ""
"When you have mastered those, you can look into the dedicated introduction "
"pages for functionality in the :ref:`user_manual`, read through the "
"overarching concepts behind (digital) painting in the :ref:"
"`general_concepts` section, or just search the :ref:`reference_manual` for "
"what a specific button does."
msgstr ""
"Wanneer u die tot u heeft genomen kunt u kijken in de introductiepagina's "
"specifiek voor functionaliteit in het :ref:`user_manual`, lees door de "
"overkoepelende concepten achter (digitaal) schilderen in d sectie :ref:"
"`general_concepts` of doorzoek gewoon de :ref:`reference_manual` naar wat "
"een specifieke knop doet."

#: ../../user_manual/getting_started.rst:15
msgid "Contents:"
msgstr "Inhoud:"
