# Lithuanian translations for Krita Manual package.
# Copyright (C) licensed under the GNU Free Documentation License 1.3+ unless stated otherwise
# This file is distributed under the same license as the Krita Manual package.
# Automatically generated, 2019.
#
msgid ""
msgstr ""
"Project-Id-Version: Krita Manual 4.1\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-08-02 03:06+0200\n"
"PO-Revision-Date: 2019-02-19 03:36+0100\n"
"Last-Translator: Automatically generated\n"
"Language-Team: lt\n"
"Language: lt\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=(n==1 ? 0 : n%10>=2 && (n%100<10 || n"
"%100>=20) ? 1 : n%10==0 || (n%100>10 && n%100<20) ? 2 : 3);\n"

#: ../../<rst_epilog>:2
msgid ""
".. image:: images/icons/Krita_mouse_left.png\n"
"   :alt: mouseleft"
msgstr ""

#: ../../<rst_epilog>:6
msgid ""
".. image:: images/icons/Krita_mouse_middle.png\n"
"   :alt: mousemiddle"
msgstr ""

#: ../../<rst_epilog>:80
msgid ""
".. image:: images/icons/pan_tool.svg\n"
"   :alt: toolpan"
msgstr ""

#: ../../reference_manual/tools/pan.rst:1
msgid "Krita's pan tool reference."
msgstr ""

#: ../../reference_manual/tools/pan.rst:11
msgid "Tools"
msgstr ""

#: ../../reference_manual/tools/pan.rst:11
msgid "Pan"
msgstr ""

#: ../../reference_manual/tools/pan.rst:16
msgid "Pan Tool"
msgstr ""

#: ../../reference_manual/tools/pan.rst:18
msgid "|toolpan|"
msgstr ""

#: ../../reference_manual/tools/pan.rst:20
msgid ""
"The pan tool allows you to pan your canvas around freely. It can be found at "
"the bottom of the toolbox, and you just it by selecting the tool, and doing |"
"mouseleft| :kbd:`+ drag` over the canvas."
msgstr ""

#: ../../reference_manual/tools/pan.rst:22
msgid ""
"There are two hotkeys associated with this tool, which makes it easier to "
"access from the other tools:"
msgstr ""

#: ../../reference_manual/tools/pan.rst:24
msgid ":kbd:`Space +` |mouseleft| :kbd:`+ drag` over the canvas."
msgstr ""

#: ../../reference_manual/tools/pan.rst:25
msgid "|mousemiddle| :kbd:`+ drag` over the canvas."
msgstr ""

#: ../../reference_manual/tools/pan.rst:27
msgid "For more information on such hotkeys, check :ref:`navigation`."
msgstr ""
