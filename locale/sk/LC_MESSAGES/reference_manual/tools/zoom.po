# translation of docs_krita_org_reference_manual___tools___zoom.po to Slovak
# Roman Paholik <wizzardsk@gmail.com>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: docs_krita_org_reference_manual___tools___zoom\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-08-02 03:06+0200\n"
"PO-Revision-Date: 2019-03-29 14:53+0100\n"
"Last-Translator: Roman Paholik <wizzardsk@gmail.com>\n"
"Language-Team: Slovak <kde-sk@linux.sk>\n"
"Language: sk\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 2.0\n"
"Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;\n"

#: ../../<rst_epilog>:2
msgid ""
".. image:: images/icons/Krita_mouse_left.png\n"
"   :alt: mouseleft"
msgstr ""
".. image:: images/icons/Krita_mouse_left.png\n"
"   :alt: mouseleft"

#: ../../<rst_epilog>:4
msgid ""
".. image:: images/icons/Krita_mouse_right.png\n"
"   :alt: mouseright"
msgstr ""
".. image:: images/icons/Krita_mouse_right.png\n"
"   :alt: mouseright"

#: ../../<rst_epilog>:6
msgid ""
".. image:: images/icons/Krita_mouse_middle.png\n"
"   :alt: mousemiddle"
msgstr ""
".. image:: images/icons/Krita_mouse_middle.png\n"
"   :alt: mousemiddle"

#: ../../<rst_epilog>:82
msgid ""
".. image:: images/icons/zoom_tool.svg\n"
"   :alt: toolzoom"
msgstr ""

#: ../../reference_manual/tools/zoom.rst:1
msgid "Krita's zoom tool reference."
msgstr ""

#: ../../reference_manual/tools/zoom.rst:11
msgid "Tools"
msgstr ""

#: ../../reference_manual/tools/zoom.rst:11
#, fuzzy
#| msgid "Zoom Tool"
msgid "Zoom"
msgstr "Nástroj priblíženia"

#: ../../reference_manual/tools/zoom.rst:16
msgid "Zoom Tool"
msgstr "Nástroj priblíženia"

#: ../../reference_manual/tools/zoom.rst:18
msgid "|toolzoom|"
msgstr ""

#: ../../reference_manual/tools/zoom.rst:20
msgid ""
"The zoom tool allows you to zoom your canvas in and out discretely. It can "
"be found at the bottom of the toolbox, and you just activate it by selecting "
"the tool, and doing |mouseleft| on the canvas will zoom in, while |"
"mouseright| will zoom out."
msgstr ""

#: ../../reference_manual/tools/zoom.rst:22
msgid "You can reverse this behavior in the :guilabel:`Tool Options`."
msgstr ""

#: ../../reference_manual/tools/zoom.rst:24
msgid ""
"There's a number of hotkeys associated with this tool, which makes it easier "
"to access from the other tools:"
msgstr ""

#: ../../reference_manual/tools/zoom.rst:26
msgid ""
":kbd:`Ctrl + Space +` |mouseleft| :kbd:`+ drag` on the canvas will zoom in "
"or out fluently."
msgstr ""

#: ../../reference_manual/tools/zoom.rst:27
msgid ""
":kbd:`Ctrl +` |mousemiddle| :kbd:`+ drag` on the canvas will zoom in or out "
"fluently."
msgstr ""

#: ../../reference_manual/tools/zoom.rst:28
msgid ""
":kbd:`Ctrl + Alt + Space +` |mouseleft| :kbd:`+ drag` on the canvas will "
"zoom in or out with discrete steps."
msgstr ""

#: ../../reference_manual/tools/zoom.rst:29
msgid ""
":kbd:`Ctrl + Alt +` |mousemiddle| :kbd:`+ drag` on the canvas will zoom in "
"or out with discrete steps."
msgstr ""

#: ../../reference_manual/tools/zoom.rst:30
msgid ":kbd:`+` will zoom in with discrete steps."
msgstr ""

#: ../../reference_manual/tools/zoom.rst:31
msgid ":kbd:`-` will zoom out with discrete steps."
msgstr ""

#: ../../reference_manual/tools/zoom.rst:32
msgid ":kbd:`1` will set the zoom to 100%."
msgstr ""

#: ../../reference_manual/tools/zoom.rst:33
msgid ""
":kbd:`2` will set the zoom so that the document fits fully into the canvas "
"area."
msgstr ""

#: ../../reference_manual/tools/zoom.rst:34
msgid ""
":kbd:`3` will set the zoom so that the document width fits fully into the "
"canvas area."
msgstr ""

#: ../../reference_manual/tools/zoom.rst:36
msgid "For more information on such hotkeys, check :ref:`navigation`."
msgstr ""
