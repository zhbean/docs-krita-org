# translation of docs_krita_org_reference_manual___layers_and_masks___transparency_masks.po to Slovak
# Roman Paholik <wizzardsk@gmail.com>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: "
"docs_krita_org_reference_manual___layers_and_masks___transparency_masks\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-05-04 03:33+0200\n"
"PO-Revision-Date: 2019-04-02 10:51+0200\n"
"Last-Translator: Roman Paholik <wizzardsk@gmail.com>\n"
"Language-Team: Slovak <kde-sk@linux.sk>\n"
"Language: sk\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 18.12.3\n"
"Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;\n"

#: ../../reference_manual/layers_and_masks/transparency_masks.rst:1
msgid "How to use transparency masks in Krita."
msgstr ""

#: ../../reference_manual/layers_and_masks/transparency_masks.rst:15
msgid "Layers"
msgstr ""

#: ../../reference_manual/layers_and_masks/transparency_masks.rst:15
msgid "Masks"
msgstr ""

#: ../../reference_manual/layers_and_masks/transparency_masks.rst:15
#, fuzzy
#| msgid "Transparency Masks"
msgid "Transparency"
msgstr "Masky priehľadnosti"

#: ../../reference_manual/layers_and_masks/transparency_masks.rst:20
msgid "Transparency Masks"
msgstr "Masky priehľadnosti"

#: ../../reference_manual/layers_and_masks/transparency_masks.rst:22
msgid ""
"The Transparency mask allows you to selectively show or hide parts of a "
"layer.  By using a mask, you are able to avoid deleting parts of an image "
"that you just might want in the future. This allows you to work non-"
"destructively."
msgstr ""

#: ../../reference_manual/layers_and_masks/transparency_masks.rst:24
msgid ""
"In addition, it allows you to do things like remove a portion of a layer in "
"the layer stack so you can see what's behind it. One example would be if you "
"wanted to replace a sky, but were unsure of how much you wanted to replace."
msgstr ""

#: ../../reference_manual/layers_and_masks/transparency_masks.rst:28
msgid "How to add a transparency mask"
msgstr ""

#: ../../reference_manual/layers_and_masks/transparency_masks.rst:30
msgid "Click on a paint layer in the layers docker."
msgstr ""

#: ../../reference_manual/layers_and_masks/transparency_masks.rst:31
msgid ""
"Click on \"+\" drop-down in the bottom left corner of the layers docker and "
"choose :menuselection:`Transparency Mask`."
msgstr ""

#: ../../reference_manual/layers_and_masks/transparency_masks.rst:32
msgid ""
"Use your preferred paint tool to paint on the canvas. Black paints "
"transparency (see-through), white paints opacity (visible). Gray values "
"paint semi-transparency."
msgstr ""

#: ../../reference_manual/layers_and_masks/transparency_masks.rst:35
msgid ""
"You can always fine-tune and edit what you want visible and any layer. If "
"you discover you've hidden part of your paint layer accidentally, you can "
"always show it again just by painting white on your transparency mask."
msgstr ""

#: ../../reference_manual/layers_and_masks/transparency_masks.rst:37
msgid ""
"This makes for a workflow that is extremely flexible and tolerant of "
"mistakes."
msgstr ""
