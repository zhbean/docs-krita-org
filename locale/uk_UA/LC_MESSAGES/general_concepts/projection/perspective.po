# Translation of docs_krita_org_general_concepts___projection___perspective.po to Ukrainian
# Copyright (C) 2019 This_file_is_part_of_KDE
# This file is distributed under the license LGPL version 2.1 or
# version 3 or later versions approved by the membership of KDE e.V.
#
# Yuri Chornoivan <yurchor@ukr.net>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: "
"docs_krita_org_general_concepts___projection___perspective\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-08-14 03:19+0200\n"
"PO-Revision-Date: 2019-08-14 08:44+0300\n"
"Last-Translator: Yuri Chornoivan <yurchor@ukr.net>\n"
"Language-Team: Ukrainian <kde-i18n-uk@kde.org>\n"
"Language: uk\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=n==1 ? 3 : n%10==1 && n%100!=11 ? 0 : n"
"%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;\n"
"X-Generator: Lokalize 19.11.70\n"

#: ../../general_concepts/projection/perspective.rst:None
msgid ""
".. image:: images/category_projection/Projection_Lens1_from_wikipedia.svg"
msgstr ""
".. image:: images/category_projection/Projection_Lens1_from_wikipedia.svg"

#: ../../general_concepts/projection/perspective.rst:None
msgid ".. image:: images/category_projection/projection-cube_12.svg"
msgstr ".. image:: images/category_projection/projection-cube_12.svg"

#: ../../general_concepts/projection/perspective.rst:None
msgid ".. image:: images/category_projection/projection-cube_13.svg"
msgstr ".. image:: images/category_projection/projection-cube_13.svg"

#: ../../general_concepts/projection/perspective.rst:None
msgid ".. image:: images/category_projection/projection_image_31.png"
msgstr ".. image:: images/category_projection/projection_image_31.png"

#: ../../general_concepts/projection/perspective.rst:None
msgid ".. image:: images/category_projection/projection_animation_03.gif"
msgstr ".. image:: images/category_projection/projection_animation_03.gif"

#: ../../general_concepts/projection/perspective.rst:None
msgid ".. image:: images/category_projection/projection_image_32.png"
msgstr ".. image:: images/category_projection/projection_image_32.png"

#: ../../general_concepts/projection/perspective.rst:None
msgid ".. image:: images/category_projection/projection_image_33.png"
msgstr ".. image:: images/category_projection/projection_image_33.png"

#: ../../general_concepts/projection/perspective.rst:None
msgid ".. image:: images/category_projection/projection_image_34.png"
msgstr ".. image:: images/category_projection/projection_image_34.png"

#: ../../general_concepts/projection/perspective.rst:None
msgid ".. image:: images/category_projection/projection_image_35.png"
msgstr ".. image:: images/category_projection/projection_image_35.png"

#: ../../general_concepts/projection/perspective.rst:None
msgid ".. image:: images/category_projection/projection_image_36.png"
msgstr ".. image:: images/category_projection/projection_image_36.png"

#: ../../general_concepts/projection/perspective.rst:None
msgid ".. image:: images/category_projection/projection_image_37.png"
msgstr ".. image:: images/category_projection/projection_image_37.png"

#: ../../general_concepts/projection/perspective.rst:1
msgid "Perspective projection."
msgstr "Проєкція перспективи."

#: ../../general_concepts/projection/perspective.rst:10
msgid ""
"This is a continuation of the :ref:`axonometric tutorial "
"<projection_axonometric>`, be sure to check it out if you get confused!"
msgstr ""
"Це продовження :ref:`підручника щодо аксонометричної проєкції "
"<projection_axonometric>`. Прочитайте його спершу, якщо щось у цьому розділі "
"видасться вам незрозумілим!"

#: ../../general_concepts/projection/perspective.rst:12
#: ../../general_concepts/projection/perspective.rst:16
msgid "Perspective Projection"
msgstr "Проєкція перспективи"

#: ../../general_concepts/projection/perspective.rst:12
msgid "Projection"
msgstr "Проєкція"

#: ../../general_concepts/projection/perspective.rst:12
msgid "Perspective"
msgstr "Перспектива"

#: ../../general_concepts/projection/perspective.rst:18
msgid ""
"So, up till now we’ve done only parallel projection. This is called like "
"that because all the projection lines we drew were parallel ones."
msgstr ""
"Отже, досі ми мали справу із паралельними проєкціями. Назва цих проєкцій "
"походить від того, що усі намальовані нами лінії проєкції були паралельними "
"між собою."

#: ../../general_concepts/projection/perspective.rst:20
msgid ""
"However, in real life we don’t have parallel projection. This is due to the "
"lens in our eyes."
msgstr ""
"Втім, у реальному світі проєкції не є паралельними. Причиною цього є лінзи "
"наших очей."

#: ../../general_concepts/projection/perspective.rst:25
msgid ""
"Convex lenses, as this lovely image from `wikipedia <https://en.wikipedia."
"org/wiki/Lens_%28optics%29>`_ shows us, have the ability to turn parallel "
"lightrays into converging ones."
msgstr ""
"Опуклі лінзи об'єктива, як показано на `чудовому зображенні у Вікіпедії "
"<https://en.wikipedia.org/wiki/Lens_%28optics%29>`_, мають властивість "
"збирати паралельні промені світла у одній точці."

#: ../../general_concepts/projection/perspective.rst:27
msgid ""
"The point where all the rays come together is called the focal point, and "
"the vanishing point in a 2d drawing is related to it as it’s the expression "
"of the maximum distortion that can be given to two parallel lines as they’re "
"skewed toward the focal point."
msgstr ""
"Точка, у якій сходяться промені називається фокальною точкою (фокусом), а "
"нескінченно віддалену точку у плоских малюнках пов'язано із цією точкою, "
"оскільки це зображення максимального викривлення, якого можна надати двом "
"паралельним лініями при перекошуванні до фокальної точки."

#: ../../general_concepts/projection/perspective.rst:29
msgid ""
"As you can see from the image, the focal point is not an end-point of the "
"rays. Rather, it is where the rays cross before diverging again… The only "
"difference is that the resulting image will be inverted. Even in our eyes "
"this inversion happens, but our brains are used to this awkwardness since "
"childhood and turn it around automatically."
msgstr ""
"Якщо можна бачити із рисунка, фокальна точка не є кінцевою точкою променів. "
"Це лише точка, проходячи крізь яку промені знову розходяться… Відмінність "
"полягає лише у тому, що зображення після цієї точки є перевернутим. Таке "
"перевертання зображення відбувається і у наших очах, але мозок звик до нього "
"з дитинства, отже виконує зворотне перевертання зображення автоматично."

#: ../../general_concepts/projection/perspective.rst:31
msgid "Let’s see if we can perspectively project our box now."
msgstr ""
"Погляньмо, чи ми зможемо створити проєкцію перспективи з нашого "
"паралелепіпеда."

#: ../../general_concepts/projection/perspective.rst:36
msgid ""
"That went pretty well. As you can see we sort of *merged* the two sides into "
"one (resulting into the purple side square) so we had an easier time "
"projecting. The projection is limited to one or two vanishing point type "
"projection, so only the horizontal lines get distorted. We can also distort "
"the vertical lines"
msgstr ""
"Вийшло доволі добре. Як можете бачити, ми певним чином «об'єднали» дві грані "
"у одну (результат показано пурпуровим бічним прямокутником), щоб нам легше "
"було проєктувати. Проєктування обмежено однією або двома проєкціями із "
"нескінченно віддаленою точкою, отже викривляються лише горизонтальні лінії. "
"Ми також можемо викривити і вертикальні лінії:"

#: ../../general_concepts/projection/perspective.rst:41
msgid ""
"… to get three-point projection, but this is a bit much. (And I totally made "
"a mistake in there…)"
msgstr ""
"…щоб отримати триточкову проєкцію, але це занадто. (І загалом неправильно…)"

#: ../../general_concepts/projection/perspective.rst:43
msgid "Let’s setup our perspective projection again…"
msgstr "Налаштуймо проєкцію перспективи ще раз…"

#: ../../general_concepts/projection/perspective.rst:48
msgid ""
"We’ll be using a single vanishing point for our focal point. A guide line "
"will be there for the projection plane, and we’re setting up horizontal and "
"vertical parallel rules to easily draw the straight lines from the view "
"plane to where they intersect."
msgstr ""
"Як фокальну точку, ми будемо використовувати одну нескінченно віддалену "
"точку. Намалюємо напрямку для площини проєктування і налаштуємо "
"горизонтальну і вертикальну паралельні лінійки для спрощення малювання "
"прямих ліній з площини перегляду до точки перетину."

#: ../../general_concepts/projection/perspective.rst:50
msgid ""
"And now the workflow in GIF format… (don’t forget you can rotate the canvas "
"with the :kbd:`4` and :kbd:`6` keys)"
msgstr ""
"А тепер, робоча процедура у форматі GIF… (не забувайте, що полотно можна "
"обертати за допомогою клавіш :kbd:`4` та :kbd:`6`)"

#: ../../general_concepts/projection/perspective.rst:55
msgid "Result:"
msgstr "Результат:"

#: ../../general_concepts/projection/perspective.rst:60
msgid "Looks pretty haughty, doesn’t he?"
msgstr "Хлопець виглядає зарозумілим, чи не так?"

#: ../../general_concepts/projection/perspective.rst:62
msgid "And again, there’s technically a simpler setup here…"
msgstr "І знову ж, технічно, можна зробити усе простіше…"

#: ../../general_concepts/projection/perspective.rst:64
msgid "Did you know you can use Krita to rotate in 3d? No?"
msgstr ""
"Чи ви знали, що Krita можна скористатися для просторового обертання? Ні?"

#: ../../general_concepts/projection/perspective.rst:69
msgid "Well, now you do."
msgstr "Гаразд, тепер ви знаєте, що це можна робити."

#: ../../general_concepts/projection/perspective.rst:71
msgid "The ortho graphics are being set to 45 and 135 degrees respectively."
msgstr "Значення для обертання встановлено на 45 і 135 градусів відповідно."

#: ../../general_concepts/projection/perspective.rst:73
msgid ""
"We draw horizontal lines on the originals, so that we can align vanishing "
"point rulers to them."
msgstr ""
"Ми намалювали горизонтальні лінії на оригіналах, щоб вирівняти за ними "
"лінійки нескінченно віддаленої точки."

#: ../../general_concepts/projection/perspective.rst:78
msgid ""
"And from this, like with the shearing method, we start drawing. (Don’t "
"forget the top-views!)"
msgstr ""
"І, починаючи звідси, подібно до методу перекошування, розпочнімо малювати. "
"(Не забудьте про вид згори!)"

#: ../../general_concepts/projection/perspective.rst:80
msgid "Which should get you something like this:"
msgstr "Що дасть вам щось таке:"

#: ../../general_concepts/projection/perspective.rst:85
msgid "But again, the regular method is actually a bit easier..."
msgstr "Втім, знову ж таки, звичайний спосіб, насправді, трохи простіший…"

#: ../../general_concepts/projection/perspective.rst:87
msgid ""
"But now you might be thinking: gee, this is a lot of work… Can’t we make it "
"easier with the computer somehow?"
msgstr ""
"Тепер, у вас може виникнути думка: «Оце тобі, доволі багато роботи… Чи не "
"можна її якось полегшити за допомогою комп'ютера?»"

#: ../../general_concepts/projection/perspective.rst:89
msgid ""
"Uhm, yes, that’s more or less why people spent time on developing 3d "
"graphics technology:"
msgstr ""
"Угу, звичайно ж, саме на це розробники витрачають час на розробку технологій "
"тривимірної графіки:"

#: ../../general_concepts/projection/perspective.rst:97
msgid ""
"(The image above is sculpted in blender using our orthographic reference)"
msgstr ""
"Наведене вище зображення оброблено у Blender за допомогою нашого "
"ортографічного еталона."

#: ../../general_concepts/projection/perspective.rst:99
msgid ""
"So let us look at what this technique can be practically used for in the "
"next part..."
msgstr ""
"На практичне використання цієї методики ми поглянемо у наступному розділі "
"підручника…"
