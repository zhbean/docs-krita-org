# Translation of docs_krita_org_user_manual___python_scripting___introduction_to_python_scripting.po to Ukrainian
# Copyright (C) 2019 This_file_is_part_of_KDE
# This file is distributed under the license LGPL version 2.1 or
# version 3 or later versions approved by the membership of KDE e.V.
#
# Yuri Chornoivan <yurchor@ukr.net>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: "
"docs_krita_org_user_manual___python_scripting___introduction_to_python_scripting\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-08-02 03:06+0200\n"
"PO-Revision-Date: 2019-08-02 11:51+0300\n"
"Last-Translator: Yuri Chornoivan <yurchor@ukr.net>\n"
"Language-Team: Ukrainian <kde-i18n-uk@kde.org>\n"
"Language: uk\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=n==1 ? 3 : n%10==1 && n%100!=11 ? 0 : n"
"%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;\n"
"X-Generator: Lokalize 19.04.0\n"

#: ../../<rst_epilog>:188
msgid "-DENABLE_PYTHON_2=ON"
msgstr "-DENABLE_PYTHON_2=ON"

#: ../../user_manual/python_scripting/introduction_to_python_scripting.rst:1
msgid "Introduction to using Krita's python plugin API."
msgstr "Вступ до використання програмного інтерфейсу додатків на Python Krita."

#: ../../user_manual/python_scripting/introduction_to_python_scripting.rst:13
msgid "Python"
msgstr "Python"

#: ../../user_manual/python_scripting/introduction_to_python_scripting.rst:13
msgid "Python Scripting"
msgstr "Сценарії на Python"

#: ../../user_manual/python_scripting/introduction_to_python_scripting.rst:13
msgid "Scripting"
msgstr "Скрипти"

#: ../../user_manual/python_scripting/introduction_to_python_scripting.rst:13
msgid "Plugin"
msgstr "Додаток"

#: ../../user_manual/python_scripting/introduction_to_python_scripting.rst:13
msgid "Debug"
msgstr "Діагностика"

#: ../../user_manual/python_scripting/introduction_to_python_scripting.rst:18
msgid "Introduction to Python Scripting"
msgstr "Вступ до створення сценаріїв на Python"

#: ../../user_manual/python_scripting/introduction_to_python_scripting.rst:22
msgid ""
"When we offered python scripting as one of Kickstarter Stretchgoals we could "
"implement next to vectors and text, it won the backer vote by a landslide. "
"Some people even only picked python and nothing else. So what exactly is "
"python scripting?"
msgstr ""
"Коли ми запропонували реалізацію підтримки сценаріїв мовою Python для "
"обробки векторних і текстових даних як одну з цілей для збирання коштів на "
"Kickstarter, кількість спонсорів була величезною. Дехто навіть хотів лише "
"підтримки Python і нічого іншого. Отже, що ж таке сценарії мовою Python?"

#: ../../user_manual/python_scripting/introduction_to_python_scripting.rst:25
msgid "What is Python Scripting?"
msgstr "Що являють собою сценарії мовою Python?"

#: ../../user_manual/python_scripting/introduction_to_python_scripting.rst:27
msgid ""
"Python is a scripting language, that can be used to automate tasks. What "
"python scripting in Krita means is that we added an API to krita, which is a "
"bit of programming that allows python to access to parts of Krita. With this "
"we can make dockers, perform menial tasks on a lot of different files and "
"even write our own exporters. People who work with computer graphics, like "
"VFX and video game artists use python a lot to make things like sprite "
"sheets, automate parts of export and more."
msgstr ""
"Python — скриптова мова, якою можна скористатися для автоматизації роботи. "
"Можливість користуватися сценаріями Python у Krita означає, що у Krita є "
"програмний інтерфейс, який уможливлює доступ до частин Krita за допомогою "
"коду мовою Python. За допомогою цього інтерфейсу ми можемо створювати бічні "
"панелі, виконувати пакетну обробку багатьох різних файлів і навіть "
"створювати власні засоби експортування даних. Ті, хто працює із комп'ютерною "
"графікою, зокрема VFX та графікою відеоігор, використовують Python для "
"виконання багатьох завдань, зокрема створення спрайтів, автоматизації "
"частину процесів експортування тощо."

#: ../../user_manual/python_scripting/introduction_to_python_scripting.rst:29
msgid ""
"It is outside the scope of this manual to teach you python itself. However, "
"as python is an extremely popular programming language and great for "
"beginners, there's tons of learning material around that can be quickly "
"found with a simple 'learn python' internet search."
msgstr ""
"У цьому розділі підручника ми не будемо вчити вас самого програмування мовою "
"Python. Втім, оскільки Python є надзвичайно популярною і зручною для "
"початківців мовою програмування, існує безліч навчальних матеріалів, які "
"можна знайти за простим пошуком у інтернеті з ключовими словами «вивчити "
"python»."

#: ../../user_manual/python_scripting/introduction_to_python_scripting.rst:31
msgid ""
"This manual will instead focus on how to use python to automate and extend "
"Krita. For that we'll first start with the basics: How to run Python "
"commands in the scripter."
msgstr ""
"Натомість у цьому розділі ми зосередимося на тому, як скористатися Python "
"для автоматизації та розширення можливостей Krita. Для цього ми розпочнемо з "
"основ — того, як віддавати команди Python у скриптері."

#: ../../user_manual/python_scripting/introduction_to_python_scripting.rst:35
msgid "How to Enable the Scripter Plugin"
msgstr "Як увімкнути додаток для роботи із сценаріями"

#: ../../user_manual/python_scripting/introduction_to_python_scripting.rst:37
msgid ""
"The scripter plugin is not necessary to use python, but it is very useful "
"for testing and playing around with python. It is a python console, written "
"in python, which can be used to write small scripts and execute them on the "
"fly."
msgstr ""
"Для користування python додаток «Скриптер» не є обов'язковим, але він є дуже "
"корисним для тестування та вправляння у python. Це консоль Python, яку "
"написано на Python і якою можна користуватися для створення невеличких "
"сценаріїв і виконання їх на льоту."

#: ../../user_manual/python_scripting/introduction_to_python_scripting.rst:39
msgid ""
"To open the scripter, navigate to :menuselection:`Tools --> Scripts --> "
"Scripter`. If you don't see it listed, go to :menuselection:`Settings --> "
"Configure Krita --> Python Plugin Manager` and toggle \"Scripter\" in the "
"list to enable it. If you don't see the scripter plugin, make sure you are "
"using an up-to-date version of Krita."
msgstr ""
"Щоб відкрити вікно скриптера, скористайтеся пунктом меню :menuselection:"
"`Інструменти --> Скрипти --> Скриптер`. Якщо цього пункту у меню немає, "
"скористайтеся пунктом меню :menuselection:`Параметри --> Налаштувати Krita --"
"> Керування додатками Python` і увімкніть «Скриптер» у списку, щоб у "
"програмі з'явився відповідний пункт меню. Якщо у списку додатків немає "
"«Скриптера», переконайтеся, що ви користуєтеся достатньо новою версією Krita."

#: ../../user_manual/python_scripting/introduction_to_python_scripting.rst:41
msgid ""
"The scripter will pop up with a text editor window on top and an output "
"window below. Input the following in the text area:"
msgstr ""
"Скриптер відкриє вікно текстового редактора згори і вікно виведення даних "
"унизу. Введіть такі команди до області редагування тексту:"

#: ../../user_manual/python_scripting/introduction_to_python_scripting.rst:47
msgid "print(\"hello world\")"
msgstr "print(\"hello world\")"

#: ../../user_manual/python_scripting/introduction_to_python_scripting.rst:48
msgid ""
"Press the big play button or press the :kbd:`Ctrl + R` shortcut to run the "
"script. Then, below, in the output area the following should show up::"
msgstr ""
"Натисніть білу кнопку відтворення або комбінацію клавіш :kbd:`Ctrl + R`, щоб "
"наказати програмі виконати сценарій. Далі, у області виведення даних має "
"з'явитися ось що::"

#: ../../user_manual/python_scripting/introduction_to_python_scripting.rst:50
msgid ""
"==== Warning: Script not saved! ====\n"
"hello world"
msgstr ""
"==== Warning: Script not saved! ====\n"
"hello world"

#: ../../user_manual/python_scripting/introduction_to_python_scripting.rst:53
msgid ""
"Now we have a console that can run functions like print() from the Python "
"environment - but how do we use it to manage Krita?"
msgstr ""
"Гаразд, маємо консоль, у якій можна запускати функції, подібні до print(), у "
"середовищі Python. Але як скористатися нею для керування Krita?"

#: ../../user_manual/python_scripting/introduction_to_python_scripting.rst:56
msgid "Running basic Krita commands"
msgstr "Виконання базових команд Krita"

#: ../../user_manual/python_scripting/introduction_to_python_scripting.rst:58
msgid ""
"To allow Python to communicate with Krita, we will use the Krita module. At "
"the top of every script, we will write: ``from krita import *``"
msgstr ""
"Щоб дозволити Python обмінюватися даними з Krita, ми скористаємося модулем "
"Krita. На початку будь-якого сценарію маємо написати: ``from krita import *``"

#: ../../user_manual/python_scripting/introduction_to_python_scripting.rst:60
msgid ""
"This allows us to talk to Krita through ``Krita.instance()``. Let's try to "
"double our coding abilities with Python."
msgstr ""
"Це надасть змогу сценарію обмінюватися даними з Krita за допомогою ``Krita."
"instance()``. Давайте спробуємо подвоїти наші можливості з програмування за "
"допомогою Python."

#: ../../user_manual/python_scripting/introduction_to_python_scripting.rst:67
msgid ""
"from krita import *\n"
"\n"
"Krita.instance().action('python_scripter').trigger()"
msgstr ""
"from krita import *\n"
"\n"
"Krita.instance().action('python_scripter').trigger()"

#: ../../user_manual/python_scripting/introduction_to_python_scripting.rst:68
msgid ""
"You should see a second scripter window open. Pretty neat! Here is a "
"slightly more advanced example."
msgstr ""
"Ви маєте побачити відкриття другого вікна скриптера. Нічогенько! Ось трохи "
"ширший приклад."

#: ../../user_manual/python_scripting/introduction_to_python_scripting.rst:76
msgid ""
"from krita import *\n"
"\n"
"d = Krita.instance().createDocument(512, 512, \"Python test document\", "
"\"RGBA\", \"U8\", \"\", 120.0)\n"
"Krita.instance().activeWindow().addView(d)"
msgstr ""
"from krita import *\n"
"\n"
"d = Krita.instance().createDocument(512, 512, \"Python test document\", "
"\"RGBA\", \"U8\", \"\", 120.0)\n"
"Krita.instance().activeWindow().addView(d)"

#: ../../user_manual/python_scripting/introduction_to_python_scripting.rst:77
msgid ""
"This will open up a new document. Clearly Python gives you quite a lot of "
"control to automate Krita. Over time we expect the community to write all "
"kinds of scripts that you can use simply by pasting them in the scripter."
msgstr ""
"Виконання цього сценарію має призвести до відкриття нового документа. "
"Очевидно, Python надає вам доволі широкі можливості з автоматизації роботи "
"Krita. З часом, ми очікуємо, що спільнота користувачів напише сценарії усіх "
"типів, щоб ви могли просто вставити текст цих сценаріїв до вікна скриптера."

#: ../../user_manual/python_scripting/introduction_to_python_scripting.rst:79
msgid ""
"But what if you want to write new commands for yourself? The best place to "
"start is very simple: search for examples written by other people! You can "
"save a lot of time if someone else has written code that you can base your "
"work on. It's also worth looking through the python plugins, which are "
"located in /share/krita/pykrita. There's also a step by step guide for :ref:"
"`krita_python_plugin_howto` here in the manual."
msgstr ""
"Але що робити, якщо ви захочете написати нові команди власноруч? Перший крок "
"є доволі простим: пошукайте приклади у коді, який вже кимось написано! Ви "
"можете заощадити багато часу, якщо знайдете чийсь інший код, на якому ви "
"зможете заснувати свою роботу. Також варто ознайомитися зі додатками Python, "
"файли яких зберігаються у каталозі /share/krita/pykrita. Крім того, у цьому "
"підручнику є покрокові настанови у розділі :ref:`krita_python_plugin_howto`."

#: ../../user_manual/python_scripting/introduction_to_python_scripting.rst:81
msgid ""
"But it's likely that you need more information. For that, we will need see "
"what's hidden behind the asterisk when you ``import * from Krita``. To learn "
"what Krita functions that are available and how to use them, you will want "
"to go for Krita API reference documentation."
msgstr ""
"Втім, ймовірно, вам знадобиться більше інформації. Щоб із нею ознайомитися, "
"вам доведеться вивчити, що ховається за символом зірочки, коли ви віддаєте "
"команду ``import * from Krita``. Щоб дізнатися про те, які функції доступні "
"у Krita і як ними користуватися, вам слід ознайомитися із довідковою "
"інформацією щодо програмного інтерфейсу Krita."

#: ../../user_manual/python_scripting/introduction_to_python_scripting.rst:84
msgid "Krita's API"
msgstr "Програмний інтерфейс Krita"

#: ../../user_manual/python_scripting/introduction_to_python_scripting.rst:86
msgid ""
"`LibKis API Overview <https://api.kde.org/extragear-api/graphics-apidocs/"
"krita/libs/libkis/html/index.html>`_"
msgstr ""
"`Огляд програмного інтерфейсу LibKis <https://api.kde.org/extragear-api/"
"graphics-apidocs/krita/libs/libkis/html/index.html>`_"

#: ../../user_manual/python_scripting/introduction_to_python_scripting.rst:87
msgid ""
"`Krita class documentation <https://api.kde.org/extragear-api/graphics-"
"apidocs/krita/libs/libkis/html/classKrita.html>`_"
msgstr ""
"`Документація до класу Krita <https://api.kde.org/extragear-api/graphics-"
"apidocs/krita/libs/libkis/html/classKrita.html>`_"

#: ../../user_manual/python_scripting/introduction_to_python_scripting.rst:89
msgid ""
"Those pages may look like a lot of jargon at first. This is because Krita's "
"API documentation comes from the underlying C++ language that Krita is "
"written in. The magic happens because of a Python tool called SIP, which "
"makes it possible for python speak in C++ and talk to Krita. The end result "
"is that when we ``import krita`` and call functions, we're actually using "
"the C++ methods listed in that documentation."
msgstr ""
"Спочатку може здатися, що ці сторінки переповнені жаргоном. Причина полягає "
"у тому, що документація з програмного інтерфейсу Krita є похідною від "
"основної мови програмування C++, якою, власне, написано саму Krita. Магічне "
"поєднання відбувається за допомогою інструмента Python, який має назву SIP і "
"який уможливлює для Python «спілкування» з кодом C++ і самою Krita. Кінцевим "
"результатом є те, що, коли ми віддаємо команду ``import krita`` і викликаємо "
"функції, насправді, ми викликаємо методи C++, про які оповідає документація "
"з програмного інтерфейсу."

#: ../../user_manual/python_scripting/introduction_to_python_scripting.rst:91
msgid ""
"Let's see how this stuff works in more detail. Let's take a look at the "
"second link, the `Krita class reference <https://api.kde.org/extragear-api/"
"graphics-apidocs/krita/libs/libkis/html/classKrita."
"html#aa55507903d088013ced2df8c74f28a63>`_. There we can see all the "
"functions available to the Krita instance. If you type dir(Krita.instance()) "
"in Python, it should match this page very closely - you can view the "
"documentation of the functions createDocument(), activeWindow(), and "
"action() which we used above."
msgstr ""
"Розгляньмо детальніше, як усе це працює. Звернімося до другого посилання, "
"`довідника з класів Krita <https://api.kde.org/extragear-api/graphics-"
"apidocs/krita/libs/libkis/html/classKrita."
"html#aa55507903d088013ced2df8c74f28a63>`_. У цьому довіднику ми можемо "
"бачити усі функції, доступні у екземплярі Krita. Якщо ви введете команду "
"dir(Krita.instance()) у Python, вона має вивести щось дуже подібне до цієї "
"сторінки — ви зможете переглянути документацію щодо функцій "
"createDocument(), activeWindow() та action(), якими ми скористалися вище."

#: ../../user_manual/python_scripting/introduction_to_python_scripting.rst:93
msgid ""
"One of the more confusing things is seeing all the C++ classes that Krita "
"uses, including the Qt classes that start with Q. But here is the beauty of "
"SIP: it tries to make the translation from these classes into Python as "
"simple and straightforward as possible. For example, you can see that the "
"function filters() returns a QStringList. However, SIP converts those "
"QStringLists into regular python list of strings!"
msgstr ""
"Однією із речей, які дещо збивають із пантелику, є те, що ми бачимо усі "
"класи C++, які використовує Krita, зокрема класи Qt, назви яких починаються "
"з Q. Але у цьому і полягає краса SIP: цей інструмент намагається стати "
"містком між цими класами і Python, якомога простішим і зрозумілішим. "
"Наприклад, ви можете бачити, що функція filters() повертає QStringList. "
"Втім, SIP перетворює такі QStringList у звичайні списки рядків Python!"

#: ../../user_manual/python_scripting/introduction_to_python_scripting.rst:100
msgid ""
"from krita import *\n"
"\n"
"print(Krita.instance().filters())"
msgstr ""
"from krita import *\n"
"\n"
"print(Krita.instance().filters())"

#: ../../user_manual/python_scripting/introduction_to_python_scripting.rst:101
msgid "Outputs as: ::"
msgstr "Виведе ::"

#: ../../user_manual/python_scripting/introduction_to_python_scripting.rst:103
msgid ""
"['asc-cdl', 'autocontrast', 'blur', 'burn', 'colorbalance', 'colortoalpha', "
"'colortransfer',\n"
"'desaturate', 'dodge', 'edge detection', 'emboss', 'emboss all directions', "
"'emboss horizontal and vertical',\n"
"'emboss horizontal only', 'emboss laplascian', 'emboss vertical only', "
"'gaussian blur', 'gaussiannoisereducer',\n"
"'gradientmap', 'halftone', 'height to normal', 'hsvadjustment', "
"'indexcolors', 'invert', 'lens blur', 'levels',\n"
"'maximize', 'mean removal', 'minimize', 'motion blur', 'noise', 'normalize', "
"'oilpaint', 'perchannel', 'phongbumpmap',\n"
"'pixelize', 'posterize', 'raindrops', 'randompick', 'roundcorners', "
"'sharpen', 'smalltiles', 'threshold', 'unsharp',\n"
"'wave', 'waveletnoisereducer']"
msgstr ""
"['asc-cdl', 'autocontrast', 'blur', 'burn', 'colorbalance', 'colortoalpha', "
"'colortransfer',\n"
"'desaturate', 'dodge', 'edge detection', 'emboss', 'emboss all directions', "
"'emboss horizontal and vertical',\n"
"'emboss horizontal only', 'emboss laplascian', 'emboss vertical only', "
"'gaussian blur', 'gaussiannoisereducer',\n"
"'gradientmap', 'halftone', 'height to normal', 'hsvadjustment', "
"'indexcolors', 'invert', 'lens blur', 'levels',\n"
"'maximize', 'mean removal', 'minimize', 'motion blur', 'noise', 'normalize', "
"'oilpaint', 'perchannel', 'phongbumpmap',\n"
"'pixelize', 'posterize', 'raindrops', 'randompick', 'roundcorners', "
"'sharpen', 'smalltiles', 'threshold', 'unsharp',\n"
"'wave', 'waveletnoisereducer']"

#: ../../user_manual/python_scripting/introduction_to_python_scripting.rst:112
msgid "However, sometimes the conversion doesn't go quite as smoothly."
msgstr ""
"Втім, іноді перетворення відбувається не так, як можна було б сподіватися."

#: ../../user_manual/python_scripting/introduction_to_python_scripting.rst:119
msgid ""
"from krita import *\n"
"\n"
"print(Krita.instance().documents())"
msgstr ""
"from krita import *\n"
"\n"
"print(Krita.instance().documents())"

#: ../../user_manual/python_scripting/introduction_to_python_scripting.rst:120
msgid "gives something like this::"
msgstr "дасть нам щось таке::"

#: ../../user_manual/python_scripting/introduction_to_python_scripting.rst:123
msgid ""
"[<PyKrita.krita.Document object at 0x7f7294630b88>,\n"
"<PyKrita.krita.Document object at 0x7f72946309d8>,\n"
"<PyKrita.krita.Document object at 0x7f7294630c18>]"
msgstr ""
"[<PyKrita.krita.Document object at 0x7f7294630b88>,\n"
"<PyKrita.krita.Document object at 0x7f72946309d8>,\n"
"<PyKrita.krita.Document object at 0x7f7294630c18>]"

#: ../../user_manual/python_scripting/introduction_to_python_scripting.rst:127
msgid ""
"It is a list of something, sure, but how to use it? If we go back to the "
"Krita apidocs page and look at the function, documents() we'll see there's "
"actually a clickable link on the 'Document' class. `If you follow that link "
"<https://api.kde.org/extragear-api/graphics-apidocs/krita/libs/libkis/html/"
"classDocument.html>`_, you'll see that the document has a function called "
"name() which returns the name of the document, and functions width() and "
"height() which return the dimensions. So if we wanted to generate an info "
"report about the documents in Krita, we could write a script like this:"
msgstr ""
"Це список чогось, але як скористатися ним? Якщо ми повернемося до сторінки "
"документації з програмного інтерфейсу Krita і поглянемо на документацію до "
"documents(), ми побачимо придатне до натискання посилання на клас "
"«Document». `Якщо перейти за цим посиланням <https://api.kde.org/extragear-"
"api/graphics-apidocs/krita/libs/libkis/html/classDocument.html>`_, можна "
"бачити, що клас документа містить функцію name(), яка повертає назву "
"документа, та функції width() (ширина) та height() (висота), які повертають "
"розмірності документа. Отже, якщо нам потрібно зібрати звіт щодо документів "
"у Krita, можна скористатися таким кодом:"

#: ../../user_manual/python_scripting/introduction_to_python_scripting.rst:136
msgid ""
"from krita import *\n"
"\n"
"for doc in Krita.instance().documents():\n"
"    print(doc.name())\n"
"    print(\" \"+str(doc.width())+\"x\"+str(doc.height()))"
msgstr ""
"from krita import *\n"
"\n"
"for doc in Krita.instance().documents():\n"
"    print(doc.name())\n"
"    print(\" \"+str(doc.width())+\"x\"+str(doc.height()))"

#: ../../user_manual/python_scripting/introduction_to_python_scripting.rst:137
msgid "We get an output like::"
msgstr "Ми отримуємо виведені результати, які подібні до таких::"

#: ../../user_manual/python_scripting/introduction_to_python_scripting.rst:139
msgid ""
"==== Warning: Script not saved! ====\n"
"Unnamed\n"
" 2480x3508\n"
"sketch21\n"
" 3508x2480\n"
"Blue morning\n"
" 1600x900"
msgstr ""
"==== Warning: Script not saved! ====\n"
"Unnamed\n"
" 2480x3508\n"
"sketch21\n"
" 3508x2480\n"
"Blue morning\n"
" 1600x900"

#: ../../user_manual/python_scripting/introduction_to_python_scripting.rst:147
msgid ""
"Hopefully this will give you an idea of how to navigate the API docs now."
msgstr ""
"Сподіваємося, ви тепер зрозуміли, як вивчати документацію із програмного "
"інтерфейсу."

#: ../../user_manual/python_scripting/introduction_to_python_scripting.rst:149
msgid ""
"Krita's API has many more classes, you can get to them by going to the top-"
"left class list, or just clicking their names to get to their API docs. The "
"functions print() or dir() are your friends here as well. This line will "
"print out a list of all the actions in Krita - you could swap in one of "
"these commands instead of 'python_scripter' in the example above."
msgstr ""
"У програмному інтерфейсі Krita передбачено багато інших класів. Ознайомитися "
"із їхнім списком можна за допомогою списку класів у верхній лівій частині "
"сторінки або натисканням назв класів для перегляду відповідної документації "
"з програмного інтерфейсу. Корисні при цьому будуть такою функції print() та "
"dir(). Ці команди виводять список усіх дій у Krita — ви можете завантажити "
"одну з цих команд замість «python_scripter» у наведеному вище прикладі."

#: ../../user_manual/python_scripting/introduction_to_python_scripting.rst:154
msgid "[print([a.objectName(), a.text()]) for a in Krita.instance().actions()]"
msgstr ""
"[print([a.objectName(), a.text()]) for a in Krita.instance().actions()]"

#: ../../user_manual/python_scripting/introduction_to_python_scripting.rst:155
msgid ""
"The Python module ``inspect`` was designed for this sort of task. Here's a "
"useful function to print info about a class to the console."
msgstr ""
"Модуль Python ``inspect`` було розроблено саме для такого типу завдань. Ось "
"корисна функція для виведення даних щодо класу до консолі:"

#: ../../user_manual/python_scripting/introduction_to_python_scripting.rst:166
msgid ""
"import inspect\n"
"def getInfo(target):\n"
"    [print(item) for item in inspect.getmembers(target) if not item[0]."
"startswith('_')]\n"
"\n"
"getInfo(Krita.instance())"
msgstr ""
"import inspect\n"
"def getInfo(target):\n"
"    [print(item) for item in inspect.getmembers(target) if not item[0]."
"startswith('_')]\n"
"\n"
"getInfo(Krita.instance())"

#: ../../user_manual/python_scripting/introduction_to_python_scripting.rst:167
msgid ""
"Finally, in addition to the LibKis documentation, the Qt documentation, "
"since Krita uses PyQt to expose nearly all of the Qt API to Python. You can "
"build entire windows with buttons and forms this way, using the very same "
"tools that Krita is using! You can read the `Qt documentation <https://doc."
"qt.io/>`_ and the `PyQt documentation <https://www.riverbankcomputing.com/"
"static/Docs/PyQt5/>`_ for more info about this, and also definitely study "
"the included plugins as well to see how they work."
msgstr ""
"Нарешті, на додачу до документації з LibKis, можна скористатися "
"документацією з Qt, оскільки Krita використовує PyQt для надання доступу до "
"майже усього програмного інтерфейсу Qt з середовища Python. За допомогою "
"цього інструментарію ви можете будувати цілі вікна із кнопками та формами, "
"використовуючи ті самі інструменти, які використовує Krita! Щоб дізнатися "
"більше, ознайомтеся із `документацією з Qt <https://doc.qt.io/>`_ та "
"`документацією з PyQt <https://www.riverbankcomputing.com/static/Docs/PyQt5/"
">`_, також, звичайно, вивчіть додатки, які включено до пакунка з Krita, а "
"також ознайомтеся із принципами їхньої роботи."

#: ../../user_manual/python_scripting/introduction_to_python_scripting.rst:171
msgid "Technical Details"
msgstr "Технічні подробиці"

#: ../../user_manual/python_scripting/introduction_to_python_scripting.rst:177
msgid "Python Scripting on Windows"
msgstr "Сценарії на Python у Windows"

#: ../../user_manual/python_scripting/introduction_to_python_scripting.rst:179
msgid ""
"To get Python scripting working on Windows 7/8/8.1, you will need to install "
"the `Universal C Runtime from Microsoft's website <https://www.microsoft.com/"
"en-us/download/details.aspx?id=48234>`_. (Windows 10 already comes with it.)"
msgstr ""
"Щоб мати змогу користуватися сценаріями Python у Windows 7/8/8.1, вам слід "
"встановити `Універсальне середовище виконання C з сайта Microsoft <https://"
"www.microsoft.com/en-us/download/details.aspx?id=48234>`_. (Це середовище "
"вже є частиною Windows 10.)"

#: ../../user_manual/python_scripting/introduction_to_python_scripting.rst:182
msgid "Python 2 and 3"
msgstr "Python 2 і 3"

#: ../../user_manual/python_scripting/introduction_to_python_scripting.rst:184
msgid "By default Krita is compiled for python 3."
msgstr "Типово, Krita зібрано для використання python 3."

#: ../../user_manual/python_scripting/introduction_to_python_scripting.rst:186
msgid ""
"However, it is possible to compile it with python 2. To do so, you will need "
"to add the following to the cmake configuration line::"
msgstr ""
"Втім, програму можна зібрати і з підтримкою python 2. Для цього вам слід "
"додати таку команду до рядка налаштовування системи збирання cmake::"
