# Translation of docs_krita_org_tutorials___eye_tracker.po to Catalan
# Copyright (C) 2019 This_file_is_part_of_KDE
# This file is distributed under the license LGPL version 2.1 or
# version 3 or later versions approved by the membership of KDE e.V.
#
# Antoni Bella Pérez <antonibella5@yahoo.com>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: tutorials\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-08-14 03:19+0200\n"
"PO-Revision-Date: 2019-08-17 18:31+0200\n"
"Last-Translator: Antoni Bella Pérez <antonibella5@yahoo.com>\n"
"Language-Team: Catalan <kde-i18n-ca@kde.org>\n"
"Language: ca\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 19.07.70\n"

#: ../../tutorials/eye_tracker.rst:None
msgid ""
".. image:: images/eyetracker_layout_screenshot.png\n"
"   :alt: Screenshot of Krita when used with an eye tracker."
msgstr ""
".. image:: images/eyetracker_layout_screenshot.png\n"
"   :alt: Captura de pantalla del Krita mentre s'empra un seguidor dels ulls."

#: ../../tutorials/eye_tracker.rst:1
msgid "Setting up Krita to use with an eye tracker. "
msgstr "Configurar el Krita per emprar un seguidor dels ulls."

#: ../../tutorials/eye_tracker.rst:1
msgid "- H. Turgut Uyar <hturgut@uyar.info> "
msgstr "- H. Turgut Uyar <hturgut@uyar.info>"

#: ../../tutorials/eye_tracker.rst:1
msgid "GNU free documentation license 1.3 or later."
msgstr "Llicència de la documentació lliure de GNU 1.3 o posterior."

#: ../../tutorials/eye_tracker.rst:10
msgid "Eye Tracker"
msgstr "Seguidor dels ulls"

#: ../../tutorials/eye_tracker.rst:10
msgid "User Interface"
msgstr "Interfície d'usuari"

#: ../../tutorials/eye_tracker.rst:10
msgid "Accessibility"
msgstr "Accessibilitat"

#: ../../tutorials/eye_tracker.rst:10
msgid "Physical Disability"
msgstr "Discapacitat física"

#: ../../tutorials/eye_tracker.rst:17
msgid "An Example Setup for Using Krita with an Eye Tracker"
msgstr ""
"Un exemple de configuració per emprar el Krita amb un seguidor dels ulls"

#: ../../tutorials/eye_tracker.rst:21
msgid ""
"This is not a reference document. It is based on the experiences of only one "
"user. The information might not be as applicable when using different eye "
"tracker devices or different control software."
msgstr ""
"Aquest no és un document de referència. Es basa en les experiències d'un sol "
"usuari. Podria ser que la informació no sigui aplicable quan s'utilitzen "
"dispositius de seguiment dels ulls o un programari de control diferents."

#: ../../tutorials/eye_tracker.rst:25
msgid ""
"Eye tracker devices are becoming more affordable and they are finding their "
"way into more computer setups. Although these devices are used by various "
"types of users, we will mainly focus on users who have physical disabilities "
"and can only use their eyes to interact with the computer."
msgstr ""
"Els dispositius de seguiment dels ulls són cada vegada més assequibles i "
"s'estan obrint camí en més configuracions d'ordinador. Tot i que aquests "
"dispositius són utilitzats per diversos tipus d'usuaris, ens centrarem "
"principalment en els usuaris que tenen discapacitats físiques i que només "
"poden emprar els seus ulls per a interactuar amb l'ordinador."

#: ../../tutorials/eye_tracker.rst:30
msgid ""
"If you don't already have experience with such a case, here are a few things "
"you'll need to know before you start:"
msgstr ""
"Si encara no teniu experiència amb algun cas com aquest, haureu de conèixer "
"algunes coses abans de començar:"

#: ../../tutorials/eye_tracker.rst:33
msgid ""
"The eye tracker needs to be properly calibrated such that the pointer will "
"be very close to the point where the user is looking at. This might be "
"difficult to achieve, especially if the positioning of the eye tracker with "
"respect to the user can not be fixed between different sessions."
msgstr ""
"El seguidor dels ulls s'ha de calibrar correctament de manera que el punter "
"estigui molt a prop del punt on l'usuari està mirant. Això pot ser difícil "
"d'aconseguir, especialment si la posició del seguidor dels ulls pel que fa a "
"l'usuari no es pot deixar fixa entre les diferents sessions."

#: ../../tutorials/eye_tracker.rst:38
msgid ""
"The lack of accuracy in control makes it nearly impossible to hit small "
"areas on the screen such as small buttons or menu items. Corners and edges "
"of the screen might be difficult to reach too. You also don't want to put "
"interface elements close to one another since it increases the chances of "
"selecting the wrong element accidentally."
msgstr ""
"La manca de precisió en el control fa que sigui gairebé impossible tocar "
"àrees petites a la pantalla, com botons petits o elements de menú. Les "
"cantonades i les vores de la pantalla també poden ser difícils d'abastar. "
"Tampoc voldreu que els elements de la interfície s'apropin entre si, ja que "
"augmentarà les possibilitats de seleccionar accidentalment l'element "
"incorrecte."

#: ../../tutorials/eye_tracker.rst:44
msgid ""
"Mouse operations like single click, double click, right click, drag and "
"drop, etc. all demand extra effort in the form of switching modes in the "
"program that controls the device. You will want to keep these switches to a "
"minimum so that the work will not be interrupted frequently."
msgstr ""
"Les operacions del ratolí, com fer un sol clic, doble clic, clic dret, "
"arrossegar i deixar anar, etc., requereixen un esforç addicional en la forma "
"d'alternar entre els modes al programa que controla el dispositiu. Haureu de "
"mantenir aquests alternadors al mínim de manera que no s'interrompi sovint "
"el treball."

#: ../../tutorials/eye_tracker.rst:49
msgid ""
"Switching the mode doesn't automatically start the operation. You need an "
"extra action for that. In our case, this action is \"dwelling\". For "
"example, to start a program, you switch to the left double click mode and "
"then dwell on the icon for the application to activate the double click. "
"Adjusting the dwell time is an important tradeoff: shorter dwell times allow "
"for faster work but are also more error-prone."
msgstr ""
"Alternar el mode no iniciarà automàticament l'operació. Necessitareu una "
"acció addicional per a fer-ho. En el nostre cas, aquesta acció és "
"«romandre». Per exemple, per iniciar un programa, canviareu al mode de doble "
"clic i després romandreu quiet sobre la icona de l'aplicació per activar el "
"doble clic. Ajustar el temps de romandre és un compromís important: temps "
"més curts permeten un treball més ràpid, però també són més propensos a "
"errors."

#: ../../tutorials/eye_tracker.rst:58
msgid "Requirements"
msgstr "Requeriments"

#: ../../tutorials/eye_tracker.rst:60
msgid ""
"Besides the *obvious* requirement of having an eye tracker device, you will "
"also need a control program that will let you interact with the device. When "
"you obtain the device, such a program will most probably be provided to you "
"but that program might not be sufficient for using the device with Krita."
msgstr ""
"A més del requisit *obvi* de tenir un dispositiu de seguiment dels ulls, "
"també necessitareu un programa de control que us permeti interactuar amb el "
"dispositiu. Quan obtingueu el dispositiu, el més probable és que se us "
"proporcioni aquest programa, però aquest programa podria no ser suficient "
"per a utilitzar el dispositiu amb el Krita."

#: ../../tutorials/eye_tracker.rst:65
msgid ""
"One of the basic functionalities of these programs is to emulate mouse "
"clicks. In our case, the program provides a hovering menu which includes "
"large buttons for switching modes between left/right mouse buttons and "
"single/double clicks. After selecting the mode, the hovering menu can be "
"collapsed so that it will leave more screen space for the application."
msgstr ""
"Una de les funcionalitats bàsiques d'aquests programes és emular els clics "
"del ratolí. En el nostre cas, el programa proporciona un menú flotant que "
"inclourà botons grans per alternar entre els modes dels botons esquerre i "
"dret del ratolí i per a fer clic i doble clic. Després de seleccionar el "
"mode, el menú flotant es podrà contraure per a deixar més espai de la "
"pantalla per a l'aplicació."

#: ../../tutorials/eye_tracker.rst:71
msgid ""
"In order to make them easier to configure and use, some programs include "
"only basic modes like single clicks. This is sufficient for many popular "
"applications like e-mail agents and browsers, but for Krita you need the "
"drag and drop mode to be able to draw. If the provided control software "
"doesn't support this mode (usually called \"mouse emulation\"), you can "
"contact the manufacturer of the device for assistance, or look for open "
"source options."
msgstr ""
"Per a facilitar la seva configuració i ús, alguns programes només inclouen "
"modes bàsics, com fer clic. Això és suficient per a moltes aplicacions "
"populars com els agents de correu electrònic i els navegadors, però per al "
"Krita necessitareu el mode arrossega i deixa anar per a poder dibuixar. Si "
"el programari de control proveït no admet aquest mode (generalment anomenat "
"«emulació del ratolí»), poseu-vos en contacte amb el fabricant del "
"dispositiu per obtenir un servei o cerqueu opcions de codi obert."

#: ../../tutorials/eye_tracker.rst:80
msgid "Starting Krita"
msgstr "Començar amb el Krita"

#: ../../tutorials/eye_tracker.rst:82
msgid ""
"Basically, setting the control program to left double click mode and "
"dwelling on the Krita icon on the desktop would be enough to start up Krita "
"but there are some issues with this:"
msgstr ""
"Bàsicament, establiu el programa de control al mode de doble clic esquerre i "
"amb aturar-vos al davant de la icona del Krita que hi ha a l'escriptori serà "
"suficient per iniciar el Krita, però hi ha alguns problemes amb això:"

#: ../../tutorials/eye_tracker.rst:86
msgid ""
"On startup, Krita asks you to choose a template. It's likely that you don't "
"want to go through this setting every time and just want to start with a "
"blank template."
msgstr ""
"Durant l'inici, el Krita us demanarà que trieu una plantilla. És probable "
"que no vulgueu passar per aquest ajustament cada vegada i que només vulgueu "
"començar amb una plantilla en blanc."

#: ../../tutorials/eye_tracker.rst:90
msgid ""
"Later, saving the document will require interacting with the file save "
"dialog which is not very friendly for this type of use."
msgstr ""
"Més tard, el desar el document requerirà interactuar amb el diàleg per a "
"desar fitxers, el qual no és gaire amigable per a aquest tipus d'ús."

#: ../../tutorials/eye_tracker.rst:93
msgid ""
"A workaround for these issues could be creating and saving a blank template "
"and running a script that will copy this template under a new name and send "
"it to Krita. Here's an example script for Windows which uses a timestamp "
"suffix to make sure that each file will have a different name (replace "
"USERNAME with the actual user name)::"
msgstr ""
"Una solució per a aquests problemes podria ser crear i desar una plantilla "
"en blanc i executar un script que copiarà aquesta plantilla amb un nom nou i "
"l'enviarà al Krita. Aquí hi ha un script d'exemple per a Windows, el qual "
"utilitza un sufix de marca de temps per assegurar-se que cada fitxer tindrà "
"un nom diferent (substituïu «NOM_USUARI» amb el nom real de l'usuari):"

#: ../../tutorials/eye_tracker.rst:99
msgid ""
"@echo off\n"
"for /f \"tokens=2 delims==\" %%a in ('wmic OS Get localdatetime /value') do "
"set \"dt=%%a\"\n"
"set \"YY=%dt:~2,2%\" & set \"YYYY=%dt:~0,4%\" & set \"MM=%dt:~4,2%\" & set "
"\"DD=%dt:~6,2%\"\n"
"set \"HH=%dt:~8,2%\" & set \"Min=%dt:~10,2%\" & set \"Sec=%dt:~12,2%\"\n"
"set \"datestamp=%YYYY%%MM%%DD%\" & set \"timestamp=%HH%%Min%%Sec%\"\n"
"set \"fullstamp=%YYYY%-%MM%-%DD%_%HH%-%Min%-%Sec%\"\n"
"set filename=USERNAME_%fullstamp%.kra\n"
"copy \"C:\\Users\\USERNAME\\Pictures\\blank.kra\" \"%filename%\"\n"
"start \"C:\\Program Files\\Krita (x64)\\bin\\krita.exe\" \"%filename%\""
msgstr ""
"@echo off\n"
"for /f \"tokens=2 delims==\" %%a in ('wmic OS Get localdatetime /value') do "
"set \"dt=%%a\"\n"
"set \"YY=%dt:~2,2%\" & set \"YYYY=%dt:~0,4%\" & set \"MM=%dt:~4,2%\" & set "
"\"DD=%dt:~6,2%\"\n"
"set \"HH=%dt:~8,2%\" & set \"Min=%dt:~10,2%\" & set \"Sec=%dt:~12,2%\"\n"
"set \"datestamp=%YYYY%%MM%%DD%\" & set \"timestamp=%HH%%Min%%Sec%\"\n"
"set \"fullstamp=%YYYY%-%MM%-%DD%_%HH%-%Min%-%Sec%\"\n"
"set filename=NOM_USUARI_%fullstamp%.kra\n"
"copy \"C:\\Users\\NOM_USUARI\\Pictures\\blank.kra\" \"%filename%\"\n"
"start \"C:\\Program Files\\Krita (x64)\\bin\\krita.exe\" \"%filename%\""

#: ../../tutorials/eye_tracker.rst:109
msgid ""
"Double clicking on this script will create a new Krita file in the same "
"folder as the script file. Since the file already has a name, the file save "
"dialog will be avoided. Combined with autosaving, this can be an efficient "
"way to save your work."
msgstr ""
"En fer doble clic sobre aquest script, es crearà un fitxer nou al Krita a la "
"mateixa carpeta que el fitxer de script. Com que el fitxer ja té un nom, "
"s'evitarà el diàleg per a desar el fitxer. Combinat amb el desament "
"automàtic, aquesta pot ser una forma eficient de desar el vostre treball."

#: ../../tutorials/eye_tracker.rst:116
msgid ""
"Storing these files directly on a cloud storage service will be even safer."
msgstr ""
"Emmagatzemar aquests fitxers directament en un servei d'emmagatzematge en el "
"núvol encara serà més segur."

#: ../../tutorials/eye_tracker.rst:118
msgid "You might also deal with some timing issues when starting Krita:"
msgstr ""
"També podríeu lidiar amb alguns problemes de temps en iniciar el Krita:"

#: ../../tutorials/eye_tracker.rst:120
msgid ""
"After the icon for Krita or for the script is double clicked and Krita "
"starts loading, lingering on the icon will start a second instance."
msgstr ""
"Després de fer doble clic a la icona del Krita o de l'script i que el Krita "
"comenci a carregar, el romandre sobre la icona iniciarà una segona instància "
"del programa Krita."

#: ../../tutorials/eye_tracker.rst:123
msgid ""
"Similarly, after double clicking, if another window is accidentally brought "
"to the foreground, Krita might start up partially visible behind that window."
msgstr ""
"De manera similar, després de fer doble clic, si una altra finestra passa "
"accidentalment a primer pla, el Krita podria iniciar-se parcialment visible "
"al darrere d'aquesta finestra."

#: ../../tutorials/eye_tracker.rst:126
msgid ""
"To prevent these problems, it will help if the users train themselves to "
"look at some harmless spot (like an empty space on the desktop) until Krita "
"is loaded."
msgstr ""
"Per evitar aquests problemes, serà útil que els usuaris s'entrenin per "
"observar algun lloc inofensiu (com un espai buit a l'escriptori) fins que es "
"carregui el Krita."

#: ../../tutorials/eye_tracker.rst:132
msgid "Layout"
msgstr "Disposició"

#: ../../tutorials/eye_tracker.rst:134
msgid ""
"Since the interface elements need to be large, you have to use the screen "
"area economically. Running in full-screen mode and getting rid of unused "
"menus and toolbars are the first steps that you can take. Here's the "
"screenshot of our layout:"
msgstr ""
"Atès que els elements de la interfície han de ser grans, haureu d'utilitzar "
"l'àrea de la pantalla de manera econòmica. Executar en el mode de pantalla "
"completa i desfer-vos dels menús i barres d'eines no utilitzats seran els "
"primers passos que podreu fer. Aquí hi ha la captura de pantalla de la meva "
"disposició:"

#: ../../tutorials/eye_tracker.rst:144
msgid ""
"You will want to put everything you need somewhere you can easily access. "
"For our drawings, the essential items are brushes and colors. So we've "
"decided to place permanent dockers for these."
msgstr ""
"Voldreu posar tot el que necessiteu en un lloc al qual si pugui accedir amb "
"facilitat. Per als meus dibuixos, els elements essencials són els pinzells i "
"colors. De manera que he decidit col·locar-los en acobladors permanents."

#: ../../tutorials/eye_tracker.rst:148
msgid ""
"Krita features many brushes but the docker has to contain a limited number "
"of those so that the brush icons can be large enough. We recommend that you "
"create :ref:`a custom brush preset to your own liking "
"<loading_saving_brushes>`."
msgstr ""
"Entre les característiques del Krita hi ha molts pinzells, però l'acoblador "
"n'haurà de contenir un nombre limitat, de manera que les icones dels "
"pinzells puguin ser prou grans. Recomanem que creeu :ref:`un pinzell "
"predefinit personalitzat al vostre gust <loading_saving_brushes>`."

#: ../../tutorials/eye_tracker.rst:152
msgid ""
"There are various tools for selecting color but most of them are not easily "
"usable since they require quite a high level of mouse control. The Python "
"Palette Docker is the simplest to use where you select from a set of "
"predefined colors, similar to brush presets. Again, similarly to brush "
"selection, it will help to create a :ref:`custom set of favorite colors "
"<palette_docker>`."
msgstr ""
"Hi ha diverses eines per a seleccionar el color, però la majoria d'elles no "
"són fàcils d'emprar, ja que requereixen un nivell molt alt de control del "
"ratolí. L'acoblador Paleta escrit en Python és el més senzill d'emprar, on "
"seleccionareu un conjunt de colors predefinits, similar als pinzells "
"predefinits. Novament, de manera semblant a la selecció del pinzell, us "
"ajudarà a crear :ref:`un conjunt personalitzat de colors preferits "
"<palette_docker>`."

#: ../../tutorials/eye_tracker.rst:158
msgid ""
"Once you are happy with your layout, another feature that will help you is "
"to lock the dockers. It's possible to accidentally close or move dockers. "
"For example, in drag and drop mode you can accidentally grab a docker and "
"drag it across the screen. To prevent this, put the following setting in "
"the :file:`kritarc` file::"
msgstr ""
"Una vegada esteu satisfet amb la vostra disposició, una altra característica "
"que us ajudarà és el bloquejar els acobladors. És possible que els tanqueu o "
"moveu de manera accidental. Per exemple, en el mode arrossega i deixa anar, "
"podríeu agafar accidentalment un acoblador i arrossegar-lo per la pantalla. "
"Per evitar això, poseu el següent ajustament al fitxer «:file:`kritarc`»:"

#: ../../tutorials/eye_tracker.rst:164
msgid "LockAllDockerPanels=true"
msgstr "LockAllDockerPanels=true"

#: ../../tutorials/eye_tracker.rst:166
msgid ""
"(Check the :ref:`KritaFAQ` for how to find the configuration kritarc file on "
"your system.)"
msgstr ""
"(Comproveu la :ref:`PMF del Krita <KritaFAQ>` per a trobar a on es desa el "
"fitxer de configuració «kritarc» al vostre sistema)."

#: ../../tutorials/eye_tracker.rst:168
msgid ""
"If you're using a hovering mouse control menu like we do, you also have to "
"figure out where to place it when it's collapsed. Put it somewhere where it "
"will be easily accessible but where it will not interfere with Krita. On the "
"screenshot you can see it at the left edge of the screen."
msgstr ""
"Si utilitzeu un menú de control del ratolí, també haureu d'esbrinar a on "
"posar-lo quan està col·lapsat. Poseu-lo en un lloc on sigui de fàcil accés "
"però on no interfereixi amb el Krita. A la captura de pantalla el veureu a "
"la vora esquerra de la pantalla."

#: ../../tutorials/eye_tracker.rst:174
msgid "Summary"
msgstr "Resum"

#: ../../tutorials/eye_tracker.rst:176
msgid "In summary, we work as explained below."
msgstr "En resum, treballarem com es descriu a continuació."

#: ../../tutorials/eye_tracker.rst:178
msgid "To start Krita:"
msgstr "Per a iniciar el Krita:"

#: ../../tutorials/eye_tracker.rst:180
msgid ""
"On the desktop, pull up the hovering mouse menu and select left double click "
"mode."
msgstr ""
"A l'escriptori, desplegueu el menú del ratolí i seleccioneu el mode de doble "
"clic esquerre."

#: ../../tutorials/eye_tracker.rst:182
msgid ""
"Double click on the new drawing creation script. Look away at some harmless "
"spot until Krita loads."
msgstr ""
"Feu doble clic sobre l'script per a la creació de dibuixos nous. Mirareu cap "
"a un altre costat, a algun lloc inofensiu, fins que es carregui el Krita."

#: ../../tutorials/eye_tracker.rst:185
msgid "Drawing with Krita:"
msgstr "Dibuixar amb el Krita:"

#: ../../tutorials/eye_tracker.rst:187 ../../tutorials/eye_tracker.rst:201
msgid "Switch to left single click mode."
msgstr "Canvieu al mode d'un sol clic esquerre."

#: ../../tutorials/eye_tracker.rst:188
msgid "Select a brush and/or color using the dockers."
msgstr "Seleccioneu un pinzell i/o un color utilitzant els acobladors."

#: ../../tutorials/eye_tracker.rst:189
msgid "Switch to drag and drop mode. You're ready to draw."
msgstr ""
"Canvieu al mode arrossega i deixa anar. Ja esteu preparat per a dibuixar."

#: ../../tutorials/eye_tracker.rst:190
msgid ""
"Go to the point where you want to start a stroke and dwell until dragging "
"starts (this emulates pressing and holding your finger on the mouse button)."
msgstr ""
"Aneu al punt on voleu iniciar un traç i romangueu-hi fins que es comenci a "
"arrossegar (això emula el prémer i mantenir premut amb el dit el botó del "
"ratolí)."

#: ../../tutorials/eye_tracker.rst:193
msgid "Draw."
msgstr "Dibuixeu."

#: ../../tutorials/eye_tracker.rst:194
msgid ""
"When you want to finish the current stroke, dwell at the ending point until "
"you get out of dragging (this emulates lifting your finger from the mouse "
"button)."
msgstr ""
"Si vulgueu finalitzar el traç actual, romangueu en el punt final fins que es "
"deixi d'arrossegar (això simula aixecar el dit del botó del ratolí)."

#: ../../tutorials/eye_tracker.rst:197
msgid "Repeat the whole process."
msgstr "Repetiu tot el procés."

#: ../../tutorials/eye_tracker.rst:199
msgid "Finishing:"
msgstr "Finalitzar:"

#: ../../tutorials/eye_tracker.rst:202
msgid "Click on the button for closing the window."
msgstr "Feu clic al botó per a tancar la finestra."

#: ../../tutorials/eye_tracker.rst:203
msgid ""
"When warned about unsaved changes, click the button for saving the file."
msgstr ""
"Quan us avisi de canvis sense desar, feu clic al botó per a desar el fitxer."
