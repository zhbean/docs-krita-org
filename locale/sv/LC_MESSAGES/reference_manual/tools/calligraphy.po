# Copyright (C) licensed under the GNU Free Documentation License 1.3+ unless stated otherwise
# This file is distributed under the same license as the Krita Manual package.
#
# Stefan Asserhäll <stefan.asserhall@bredband.net>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: Krita Manual 4.1\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-05-04 03:33+0200\n"
"PO-Revision-Date: 2019-06-12 07:24+0100\n"
"Last-Translator: Stefan Asserhäll <stefan.asserhall@bredband.net>\n"
"Language-Team: Swedish <kde-i18n-doc@kde.org>\n"
"Language: sv\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 2.0\n"

#: ../../<rst_epilog>:16
msgid ""
".. image:: images/icons/calligraphy_tool.svg\n"
"   :alt: toolcalligraphy"
msgstr ""
".. image:: images/icons/calligraphy_tool.svg\n"
"   :alt: Kalligrafiverktyg"

#: ../../reference_manual/tools/calligraphy.rst:1
msgid "Krita's calligraphy tool reference."
msgstr "Referens för Kritas kalligrafiverktyg."

#: ../../reference_manual/tools/calligraphy.rst:11
msgid "Tools"
msgstr "Verktyg"

#: ../../reference_manual/tools/calligraphy.rst:11
msgid "Vector"
msgstr "Vektor"

#: ../../reference_manual/tools/calligraphy.rst:11
msgid "Path"
msgstr "Kontur"

#: ../../reference_manual/tools/calligraphy.rst:11
msgid "Variable Width Stroke"
msgstr "Linjer med variabel bredd"

#: ../../reference_manual/tools/calligraphy.rst:11
msgid "Calligraphy"
msgstr "Kalligrafi"

#: ../../reference_manual/tools/calligraphy.rst:16
msgid "Calligraphy Tool"
msgstr "Kalligrafiverktyg"

#: ../../reference_manual/tools/calligraphy.rst:18
msgid "|toolcalligraphy|"
msgstr "|toolcalligraphy|"

#: ../../reference_manual/tools/calligraphy.rst:20
msgid ""
"The Calligraphy tool allows for variable width lines, with input managed by "
"the tablet. Press down with the stylus/left mouse button on the canvas to "
"make a line, lifting the stylus/mouse button ends the stroke."
msgstr ""
"Kalligrafiverktyget tillåter linjer med varierande bredd, men inmatning "
"hanterad av ritplattan. Tryck ner med pennan eller vänster musknapp på duken "
"för att skapa en linje. När pennan eller musknappen släpps avslutas strecket."

#: ../../reference_manual/tools/calligraphy.rst:24
msgid "Tool Options"
msgstr "Verktygsalternativ"

#: ../../reference_manual/tools/calligraphy.rst:26
msgid "**Fill**"
msgstr "**Fyll**"

#: ../../reference_manual/tools/calligraphy.rst:28
msgid "Doesn't actually do anything."
msgstr "Gör egentligen ingenting."

#: ../../reference_manual/tools/calligraphy.rst:30
msgid "**Calligraphy**"
msgstr "**Kalligrafi**"

#: ../../reference_manual/tools/calligraphy.rst:32
msgid ""
"The drop-down menu holds your saved presets, the :guilabel:`Save` button "
"next to it allows you to save presets."
msgstr ""
"Kombinationsmenyn innehåller sparade förinställningar, och knappen :guilabel:"
"`Spara` intill den låter dig spara förinställningar."

#: ../../reference_manual/tools/calligraphy.rst:34
msgid "Follow Selected Path"
msgstr "Följ vald kontur"

#: ../../reference_manual/tools/calligraphy.rst:35
msgid ""
"If a stroke has been selected with the default tool, the calligraphy tool "
"will follow this path."
msgstr ""
"Om ett streck har valts med standardverktyget, följer kalligrafiverktyget "
"den konturen."

#: ../../reference_manual/tools/calligraphy.rst:36
msgid "Use Tablet Pressure"
msgstr "Använd tryck mot ritplatta"

#: ../../reference_manual/tools/calligraphy.rst:37
msgid "Uses tablet pressure to control the stroke width."
msgstr "Använder tryck mot ritplattan för att styra streckbredden."

#: ../../reference_manual/tools/calligraphy.rst:38
msgid "Thinning"
msgstr "Avtunning"

#: ../../reference_manual/tools/calligraphy.rst:39
msgid ""
"This allows you to set how much thinner a line becomes when speeding up the "
"stroke. Using a negative value makes it thicker."
msgstr ""
"Låter dig ställa in hur mycket tunnare en linje blir när strecket snabbas "
"upp. Använd ett negativt värde för att göra det tjockare."

#: ../../reference_manual/tools/calligraphy.rst:40
msgid "Width"
msgstr "Bredd"

#: ../../reference_manual/tools/calligraphy.rst:41
msgid "Base width for the stroke."
msgstr "Streckets basbredd."

#: ../../reference_manual/tools/calligraphy.rst:42
msgid "Use Tablet Angle"
msgstr "Använd vinkel mot ritplatta"

#: ../../reference_manual/tools/calligraphy.rst:43
msgid ""
"Allows you to use the tablet angle to control the stroke, only works for "
"tablets supporting it."
msgstr ""
"Låter dig använda ritplattans vinkel för att styra strecket. Fungerar bara "
"med ritplattor som stöder det."

#: ../../reference_manual/tools/calligraphy.rst:44
msgid "Angle"
msgstr "Vinkel"

#: ../../reference_manual/tools/calligraphy.rst:45
msgid "The angle of the dab."
msgstr "Klickets vinkel."

#: ../../reference_manual/tools/calligraphy.rst:46
msgid "Fixation"
msgstr "Fixering"

#: ../../reference_manual/tools/calligraphy.rst:47
msgid "The ratio of the dab. 1 is thin, 0 is round."
msgstr "Klickets förhållande: 1 är tunt, 0 är runt."

#: ../../reference_manual/tools/calligraphy.rst:48
msgid "Caps"
msgstr "Ändar"

#: ../../reference_manual/tools/calligraphy.rst:49
msgid "Whether or not an stroke will end with a rounding or flat."
msgstr "Om ett streck slutar med avrundning eller plant."

#: ../../reference_manual/tools/calligraphy.rst:50
msgid "Mass"
msgstr "Massa"

#: ../../reference_manual/tools/calligraphy.rst:51
msgid ""
"How much weight the stroke has. With drag set to 0, high mass increases the "
"'orbit'."
msgstr ""
"Hur mycket vikt strecket har. Med Dra inställt till 0, ökar hög massa "
"'omloppsbanan'."

#: ../../reference_manual/tools/calligraphy.rst:53
msgid "Drag"
msgstr "Dra"

#: ../../reference_manual/tools/calligraphy.rst:53
msgid ""
"How much the stroke follows the cursor, when set to 0 the stroke will orbit "
"around the cursor path."
msgstr ""
"Hur mycket markören följer strecken: när det är inställt till 0 går strecket "
"i en bana omkring markörens väg."

#: ../../reference_manual/tools/calligraphy.rst:57
msgid ""
"The calligraphy tool can be edited by the edit-line tool, but currently you "
"can't add or remove nodes without converting it to a normal path."
msgstr ""
"Kalligrafiverktyget kan redigeras av linjeredigeringsverktyget, men för "
"närvarande kan man inte lägga till eller ta bort noder utan att konvertera "
"till en normal kontur."
