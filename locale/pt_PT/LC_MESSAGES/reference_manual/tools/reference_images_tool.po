# SOME DESCRIPTIVE TITLE.
# Copyright (C) licensed under the GNU Free Documentation License 1.3+ unless stated otherwise
# This file is distributed under the same license as the Krita Manual package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: Krita Manual 4.1\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-08-02 03:06+0200\n"
"PO-Revision-Date: 2019-08-03 10:28+0100\n"
"Last-Translator: José Nuno Coelho Pires <zepires@gmail.com>\n"
"Language-Team: Portuguese <kde-i18n-pt@kde.org>\n"
"Language: pt\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-POFile-SpellExtra: icons toolreference menuselection image\n"
"X-POFile-SpellExtra: Kritamouseleft referenceimagestool Krita Del images\n"
"X-POFile-SpellExtra: alt kra mouseleft kbd\n"

#: ../../<rst_epilog>:2
msgid ""
".. image:: images/icons/Krita_mouse_left.png\n"
"   :alt: mouseleft"
msgstr ""
".. image:: images/icons/Krita_mouse_left.png\n"
"   :alt: botão esquerdo"

#: ../../<rst_epilog>:84
msgid ""
".. image:: images/icons/reference_images_tool.svg\n"
"   :alt: toolreference"
msgstr ""
".. image:: images/icons/reference_images_tool.svg\n"
"   :alt: ferramenta de referência"

#: ../../reference_manual/tools/reference_images_tool.rst:1
msgid "The reference images tool"
msgstr "A ferramenta de imagens de referência"

#: ../../reference_manual/tools/reference_images_tool.rst:10
msgid "Tools"
msgstr "Ferramentas"

#: ../../reference_manual/tools/reference_images_tool.rst:10
msgid "Reference"
msgstr "Referência"

#: ../../reference_manual/tools/reference_images_tool.rst:15
msgid "Reference Images Tool"
msgstr "Ferramenta de Imagens de Referência"

#: ../../reference_manual/tools/reference_images_tool.rst:17
msgid "|toolreference|"
msgstr "|toolreference|"

#: ../../reference_manual/tools/reference_images_tool.rst:21
msgid ""
"The reference images tool is a replacement for the reference images docker. "
"You can use it to load images from your disk as reference, which can then be "
"moved around freely on the canvas and placed wherever."
msgstr ""
"A ferramenta de imagens de referência é um substituto para a área de imagens "
"de referência. Podẽ-la-á usar para carregar imagens do seu disco por "
"referência, e que possam ser movidas à vontade na área de desenho e "
"colocadas em qualquer lado."

#: ../../reference_manual/tools/reference_images_tool.rst:24
msgid "Tool Options"
msgstr "Opções da Ferramenta"

#: ../../reference_manual/tools/reference_images_tool.rst:26
msgid "Add reference image"
msgstr "Adicionar uma imagem de referência"

#: ../../reference_manual/tools/reference_images_tool.rst:27
msgid "Load a single image to display on the canvas."
msgstr "Carrega uma única imagem para a mostrar na área de desenho."

#: ../../reference_manual/tools/reference_images_tool.rst:28
msgid "Load Set"
msgstr "Carregar um Conjunto"

#: ../../reference_manual/tools/reference_images_tool.rst:29
msgid "Load a set of reference images."
msgstr "Carrega um conjunto de imagens de referência."

#: ../../reference_manual/tools/reference_images_tool.rst:30
msgid "Save Set"
msgstr "Gravar o Conjunto"

#: ../../reference_manual/tools/reference_images_tool.rst:31
msgid "Save a set of reference images."
msgstr "Grava um dado conjunto de imagens de referência."

#: ../../reference_manual/tools/reference_images_tool.rst:32
msgid "Delete all reference images"
msgstr "Apagar todas as imagens de referência"

#: ../../reference_manual/tools/reference_images_tool.rst:33
msgid "Delete all the reference images"
msgstr "Apaga todas as imagens de referência"

#: ../../reference_manual/tools/reference_images_tool.rst:34
msgid "Keep aspect ratio"
msgstr "Manter as proporções"

#: ../../reference_manual/tools/reference_images_tool.rst:35
msgid "When toggled this will force the image to not get distorted."
msgstr ""
"Se estiver activada esta opção, isto irá forçar a imagem a não ficar "
"distorcida."

#: ../../reference_manual/tools/reference_images_tool.rst:36
msgid "Opacity"
msgstr "Opacidade"

#: ../../reference_manual/tools/reference_images_tool.rst:37
msgid "Lower the opacity."
msgstr "Baixa a opacidade."

#: ../../reference_manual/tools/reference_images_tool.rst:38
msgid "Saturation"
msgstr "Saturação"

#: ../../reference_manual/tools/reference_images_tool.rst:39
msgid ""
"Desaturate the image. This is useful if you only want to focus on the light/"
"shadow instead of getting distracted by the colors."
msgstr ""
"Reduz a saturação da imagem. Isto é útil se só se quiser focar focar na luz/"
"sombra, em vez de ficar distraído pelas cores."

#: ../../reference_manual/tools/reference_images_tool.rst:41
msgid "How is the reference image stored."
msgstr "Como é guardada a imagem de referência."

#: ../../reference_manual/tools/reference_images_tool.rst:43
msgid "Embed to \\*.kra"
msgstr "Incorporar no \\*.kra"

#: ../../reference_manual/tools/reference_images_tool.rst:44
msgid ""
"Store this reference image into the kra file. This is recommended for small "
"vital files you'd easily lose track of otherwise."
msgstr ""
"Guarda esta imagem de referência no ficheiro .kra. Isto é recomendado para "
"pequenos ficheiros vitais que poderia perder o registo facilmente."

#: ../../reference_manual/tools/reference_images_tool.rst:46
msgid "Storage mode"
msgstr "Modo de armazenamento"

#: ../../reference_manual/tools/reference_images_tool.rst:46
msgid "Link to external file."
msgstr "Ligação ao ficheiro externo."

#: ../../reference_manual/tools/reference_images_tool.rst:46
msgid ""
"Only link to the reference image, krita will open it from the disk everytime "
"it loads this file. This is recommended for big files, or files that change "
"a lot."
msgstr ""
"Só cria uma ligação à imagem de referência, sendo o Krita abri-lo-á sempre "
"que carregar este ficheiro do disco. Isto é recomendado para ficheiros "
"grandes ou para ficheiros que mudem com frequência."

#: ../../reference_manual/tools/reference_images_tool.rst:48
msgid ""
"You can move around reference images by selecting them with |mouseleft|, and "
"dragging them. You can rotate reference images by holding the cursor close "
"to the outside of the corners till the rotate cursor appears, while tilting "
"is done by holding the cursor close to the outside of the middle nodes. "
"Resizing can be done by dragging the nodes. You can delete a single "
"reference image by clicking it and pressing the :kbd:`Del` key. You can "
"select multiple reference images with the :kbd:`Shift` key and perform all "
"of these actions."
msgstr ""
"Poderá movimentar as imagens de referência à vontade, seleccionando-as com o "
"|mouseleft| e arrastando-as. Poderá rodar as imagens de referência se "
"colocar o cursor próximo do exterior dos cantos até que apareça o cursor de "
"rotação, podendo depois desviá-las se aproximar o cursor perto do exterior "
"dos nós do meio. O dimensionamento poderá ser feito se arrastar os nós. "
"Poderá apagar uma única imagem de referência se carregar nela e carregar em :"
"kbd:`Del`. Poderá seleccionar várias imagens de referência com o :kbd:"
"`Shift` e efectuar todas essas acções."

#: ../../reference_manual/tools/reference_images_tool.rst:50
msgid ""
"To hide all reference images temporarily use :menuselection:`View --> Show "
"Reference Images`."
msgstr ""
"Para esconder todas as imagens de referência temporariamente, use a opção :"
"menuselection:`Ver --> Mostrar as Imagens de Referência`."
