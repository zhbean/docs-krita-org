# SOME DESCRIPTIVE TITLE.
# Copyright (C) licensed under the GNU Free Documentation License 1.3+ unless stated otherwise
# This file is distributed under the same license as the Krita Manual package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: Krita Manual 4.1\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-08-02 03:06+0200\n"
"PO-Revision-Date: 2019-08-03 14:30+0100\n"
"Last-Translator: José Nuno Coelho Pires <zepires@gmail.com>\n"
"Language-Team: Portuguese <kde-i18n-pt@kde.org>\n"
"Language: pt\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-POFile-SpellExtra: node en extension orthogonal Krita Scribus kbd Snap\n"
"X-POFile-SpellExtra: images intersection image px snapping\n"

#: ../../user_manual/snapping.rst:1
msgid "How to use the snapping functionality in Krita."
msgstr "Como usar a funcionalidade de ajuste no Krita."

#: ../../user_manual/snapping.rst:10 ../../user_manual/snapping.rst:43
msgid "Guides"
msgstr "Guias"

#: ../../user_manual/snapping.rst:10
msgid "Snap"
msgstr "Ajustar"

#: ../../user_manual/snapping.rst:10
msgid "Vector"
msgstr "Vector"

#: ../../user_manual/snapping.rst:15
msgid "Snapping"
msgstr "Ajuste"

#: ../../user_manual/snapping.rst:17
msgid ""
"In Krita 3.0, we now have functionality for Grids and Guides, but of course, "
"this functionality is by itself not that interesting without snapping."
msgstr ""
"No Krita 3.0, agora temos a funcionalidade para as Grelhas e Guias mas, como "
"é óbvio, esta funcionalidade por si só não é tão interessante sem o ajuste."

#: ../../user_manual/snapping.rst:21
msgid ""
"Snapping is the ability to have Krita automatically align a selection or "
"shape to the grids and guides, document center and document edges. For "
"Vector layers, this goes even a step further, and we can let you snap to "
"bounding boxes, intersections, extrapolated lines and more."
msgstr ""
"O ajuste é a capacidade de fazer com que o Krita alinhe automaticamente uma "
"selecção ou forma às grelhas e guias, ao centro do documento ou aos seus "
"extremos. Para as camadas vectoriais, isto vai ainda mais além, permitindo-"
"lhe ajustar às áreas envolventes, intersecções, linhas extrapoladas, entre "
"outras."

#: ../../user_manual/snapping.rst:26
msgid ""
"All of these can be toggled using the snap pop-up menu which is assigned to :"
"kbd:`Shift + S` shortcut."
msgstr ""
"Todos estes podem ser activados/desactivados com o menu de ajuste, cujo "
"atalho é o :kbd:`Shift + S`."

#: ../../user_manual/snapping.rst:29
msgid "Now, let us go over what each option means:"
msgstr "Agora, vamos ver o que significa cada opção:"

#: ../../user_manual/snapping.rst:32
msgid ""
"This will snap the cursor to the current grid, as configured in the grid "
"docker. This doesn’t need the grid to be visible. Grids are saved per "
"document, making this useful for aligning your art work to grids, as is the "
"case for game sprites and grid-based designs."
msgstr ""
"Isto irá ajustar o cursor à grelha actual, tal como configurado na área da "
"grelha. Isto não precisa que a grelha esteja visível. As grelhas são "
"gravadas por documento, o que torna isto útil para alinhar as suas obras às "
"grelhas, como é o caso das imagens de personagens de jogos e desenhos "
"baseados em grelhas."

#: ../../user_manual/snapping.rst:34
msgid "Grids"
msgstr "Grelhas"

#: ../../user_manual/snapping.rst:37
msgid "Pixel"
msgstr "Pixel"

#: ../../user_manual/snapping.rst:37
msgid ""
"This allows to snap to every pixel under the cursor. Similar to Grid "
"Snapping but with a grid having spacing = 1px and offset = 0px."
msgstr ""
"Isto permite ajustar a qualquer pixel debaixo do cursor. É semelhante ao "
"Ajuste à Grelha mas com uma grelha que tem um espaço = 1px e um deslocamento "
"= 0px."

#: ../../user_manual/snapping.rst:40
msgid ""
"This allows you to snap to guides, which can be dragged out from the ruler. "
"Guides do not need to be visible for this, and are saved per document. This "
"is useful for comic panels and similar print-layouts, though we recommend "
"Scribus for more intensive work."
msgstr ""
"Isto permite-lhe ajustar às guias, que podem ser arrastadas a partir da "
"guia. As guias não precisam estar visíveis para isto e são gravadas por cada "
"documento. Isto é útil para os painéis de bandas desenhadas e outros "
"formatos de impressão semelhantes, ainda que recomendemos o Scribus para um "
"trabalho mais intensivo."

#: ../../user_manual/snapping.rst:46
msgid ".. image:: images/snapping/Snap-orthogonal.png"
msgstr ".. image:: images/snapping/Snap-orthogonal.png"

#: ../../user_manual/snapping.rst:48
msgid ""
"This allows you to snap to a horizontal or vertical line from existing "
"vector objects’s nodes (Unless dealing with resizing the height or width "
"only, in which case you can drag the cursor over the path). This is useful "
"for aligning object horizontally or vertically, like with comic panels."
msgstr ""
"Isto permite-lhe ajustar a uma linha horizontal ou vertical a partir dos nós "
"dos objectos vectoriais existentes (a menos que esteja a lidar apenas com o "
"ajuste da largura ou altura, onde nesse caso poderá arrastar o cursor pelo "
"caminho). Isto é útil para alinhar o objecto na horizontal ou vertical, como "
"acontece nos painéis de banda desenhada."

#: ../../user_manual/snapping.rst:52
msgid "Orthogonal (Vector Only)"
msgstr "Ortogonal (Apenas Vectorial)"

#: ../../user_manual/snapping.rst:55
msgid ".. image:: images/snapping/Snap-node.png"
msgstr ".. image:: images/snapping/Snap-node.png"

#: ../../user_manual/snapping.rst:57
msgid "Node (Vector Only)"
msgstr "Nó (Apenas Vectorial)"

#: ../../user_manual/snapping.rst:57
msgid "This snaps a vector node or an object to the nodes of another path."
msgstr ""
"Isto ajusta um nó vectorial ou um objecto para os nós de outro caminho."

#: ../../user_manual/snapping.rst:60
msgid ".. image:: images/snapping/Snap-extension.png"
msgstr ".. image:: images/snapping/Snap-extension.png"

#: ../../user_manual/snapping.rst:62
msgid ""
"When we draw an open path, the last nodes on either side can be "
"mathematically extended. This option allows you to snap to that. The "
"direction of the node depends on its side handles in path editing mode."
msgstr ""
"Quando desenharmos um caminho aberto, os últimos nós em cada extremo podem "
"ser estendidos de forma matemática. Esta opção permite-lhe ajustar a isso. A "
"direcção do nó depende das suas pegas de tamanho, no caso do modo de edição "
"de caminhos."

#: ../../user_manual/snapping.rst:65
msgid "Extension (Vector Only)"
msgstr "Extensão (Apenas Vectorial)"

#: ../../user_manual/snapping.rst:68
msgid ".. image:: images/snapping/Snap-intersection.png"
msgstr ".. image:: images/snapping/Snap-intersection.png"

#: ../../user_manual/snapping.rst:69
msgid "Intersection (Vector Only)"
msgstr "Intersecção (Apenas Vectorial)"

#: ../../user_manual/snapping.rst:70
msgid "This allows you to snap to an intersection of two vectors."
msgstr "Isto permite-lhe ajustar a uma intersecção de dois vectores."

#: ../../user_manual/snapping.rst:71
msgid "Bounding box (Vector Only)"
msgstr "Área envolvente (Apenas Vectorial)"

#: ../../user_manual/snapping.rst:72
msgid "This allows you to snap to the bounding box of a vector shape."
msgstr "Isto permite-lhe ajustar à área envolvente de uma forma vectorial."

#: ../../user_manual/snapping.rst:74
msgid "Image bounds"
msgstr "Limites da imagem"

#: ../../user_manual/snapping.rst:74
msgid "Allows you to snap to the vertical and horizontal borders of an image."
msgstr ""
"Permite-lhe ajustar aos contornos horizontais e verticais de uma imagem."

#: ../../user_manual/snapping.rst:77
msgid "Allows you to snap to the horizontal and vertical center of an image."
msgstr "Permite-lhe ajustar ao centro horizontal e vertical de uma imagem."

#: ../../user_manual/snapping.rst:78
msgid "Image center"
msgstr "Centro da imagem"

#: ../../user_manual/snapping.rst:80
msgid "The snap works for the following tools:"
msgstr "O ajuste funciona para as seguintes ferramentas:"

#: ../../user_manual/snapping.rst:82
msgid "Straight line"
msgstr "Linha recta"

#: ../../user_manual/snapping.rst:83
msgid "Rectangle"
msgstr "Rectângulo"

#: ../../user_manual/snapping.rst:84
msgid "Ellipse"
msgstr "Elipse"

#: ../../user_manual/snapping.rst:85
msgid "Polyline"
msgstr "Linha Poligonal"

#: ../../user_manual/snapping.rst:86
msgid "Path"
msgstr "Localização"

#: ../../user_manual/snapping.rst:87
msgid "Freehand path"
msgstr "Desenho livre de caminhos"

#: ../../user_manual/snapping.rst:88
msgid "Polygon"
msgstr "Polígono"

#: ../../user_manual/snapping.rst:89
msgid "Gradient"
msgstr "Gradiente"

#: ../../user_manual/snapping.rst:90
msgid "Shape Handling tool"
msgstr "Ferramenta de Tratamento de Formas"

#: ../../user_manual/snapping.rst:91
msgid "The Text-tool"
msgstr "A ferramenta de Texto"

#: ../../user_manual/snapping.rst:92
msgid "Assistant editing tools"
msgstr "Ferramentas assistentes de edição"

#: ../../user_manual/snapping.rst:93
msgid ""
"The move tool (note that it snaps to the cursor position and not the "
"bounding box of the layer, selection or whatever you are trying to move)"
msgstr ""
"A ferramenta para Mover (lembre-se que ajusta à posição do cursor e não à "
"área envolvente da camada, selecção ou o que tentar mover)"

#: ../../user_manual/snapping.rst:96
msgid "The Transform tool"
msgstr "A ferramenta de Transformação"

#: ../../user_manual/snapping.rst:97
msgid "Rectangle select"
msgstr "Selecção rectangular"

#: ../../user_manual/snapping.rst:98
msgid "Elliptical select"
msgstr "Selecção elíptica"

#: ../../user_manual/snapping.rst:99
msgid "Polygonal select"
msgstr "Selecção poligonal"

#: ../../user_manual/snapping.rst:100
msgid "Path select"
msgstr "Selecção por caminhos"

#: ../../user_manual/snapping.rst:101
msgid "Guides themselves can be snapped to grids and vectors"
msgstr "As guias em si podem ser ajustadas às grelhas e as vectores"

#: ../../user_manual/snapping.rst:103
msgid ""
"Snapping doesn’t have a sensitivity yet, and by default is set to 10 screen "
"pixels."
msgstr ""
"O ajuste ainda não tem sensibilidade e, por omissão, está configurada como "
"10 pixels do ecrã."
